package com.exadel.tests.qrcode_scaner.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.exadel.tests.qrcode_scaner.R;


/****
 * dialog for promt messages 
 * has TExtview and close button
 * @author Apai
 *
 */
public class PromtDialog extends DisFragmentDialog {
	public static final String TAG = "PROMT";
	private static final String PROMT_MSG = "msg";
	
	/***
	 * fabric method for create new instance
	 * @param msg
	 * @return new instance of dialog
	 */
	public static PromtDialog newInstance(String msg){
		PromtDialog dlg = new PromtDialog();
		Bundle bundle = new Bundle();
		bundle.putString(PROMT_MSG, msg);
		dlg.setArguments(bundle);
		return dlg;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		int style = DialogFragment.STYLE_NORMAL;
		
		this.setRetainInstance(true);
		setStyle(style,0);
		super.onCreate(savedInstanceState);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if (getArguments()!=null&&getArguments().containsKey(PROMT_MSG))
			builder.setMessage(getArguments().getString(PROMT_MSG));
		builder.setPositiveButton(R.string.close, onClose);
		return builder.create();
	}
	OnClickListener onClose = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dismiss();
			
		}
		

	};
}
