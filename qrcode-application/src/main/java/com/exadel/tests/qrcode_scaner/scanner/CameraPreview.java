 
package com.exadel.tests.qrcode_scaner.scanner;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;


/** 
 * A basic Camera preview class 
 * */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private PreviewCallback previewCallback;
    private AutoFocusCallback autoFocusCallback;
    float mStartTouchX=0 ;
    public volatile boolean  previewing = true;
    
    public CameraPreview(Context context){
        super(context);  
    }
	
    

	public CameraPreview(Context context, Camera camera,PreviewCallback previewCb,AutoFocusCallback autoFocusCb) {
        super(context);
        
        mCamera = camera;
        previewCallback = previewCb;
        autoFocusCallback = autoFocusCb;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);

        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
    	
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.stopPreview();
            mCamera.setPreviewCallback(previewCallback);
            mCamera.setDisplayOrientation(90);
        } catch (IOException e) {
            Log.e("DBG", "Error setting camera preview: " + e.getMessage());
        } catch (Exception e){
        	Log.e("DBG", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Camera preview released in activity
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        /*
         * If your preview can change or rotate, take care of those events here.
         * Make sure to stop the preview before resizing or reformatting it.
         */
    	Log.e("cameraview" , "surfaceChanged");
        if (mHolder.getSurface() == null)       return;
        try{
        	mCamera.startPreview();
        	previewing = true;
       
        	mCamera.autoFocus(autoFocusCallback);
        }catch(Exception e){
        	e.printStackTrace();
        }
        
    }

	public void onPause(){

        try {
        	previewing = false;
            mCamera.stopPreview();
            mCamera.setPreviewDisplay(null);
            mCamera.setPreviewCallback(null);
        } catch (Exception e){
        	e.printStackTrace();
        }		
	}
	public void onResume(){
		Log.e("cameraview","onResume");
        try {
            // Hard code camera surface rotation 90 degs to match Activity view in portrait
        	previewing = true;
        	mCamera.stopPreview();
            mCamera.setPreviewDisplay(mHolder);
            mCamera.setPreviewCallback(previewCallback);
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
            mCamera.autoFocus(autoFocusCallback);
        } catch (Exception e){
            Log.d("DBG", "Error starting camera preview: " + e.getMessage());
        }		
	}

}
