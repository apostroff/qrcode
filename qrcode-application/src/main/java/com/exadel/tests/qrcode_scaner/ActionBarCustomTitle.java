package com.exadel.tests.qrcode_scaner;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.exadel.tests.qrcode_scaner.R;

/***
 * Class used for action Bar Sherlock as top view with home button  , Title and setting button
 * @author apai
 *
 */

public class ActionBarCustomTitle extends LinearLayout {
	public CustomTitleListener listener;
	
	
	public interface CustomTitleListener{
		public void onHomeClick();
		public void onSettingsClick();
	}

	public ActionBarCustomTitle(Context context) {
		super(context);		
		LayoutInflater.from(context).inflate(R.layout.abs_custom_title, this, true);
		findViewById(R.id.abs_top_im_home).setOnClickListener(onHome);		
		findViewById(R.id.abs_top_im_settings).setOnClickListener(onSettings);		
	}
	
	public ActionBarCustomTitle(Context context , String title) {
		super(context);		
		setTitle(title );
	}
	
	private OnClickListener onHome = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (listener!=null)  listener.onHomeClick();
			
		}
	};
	
	private OnClickListener onSettings = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (listener!=null)  listener.onSettingsClick();
			
		}
	};
	
	public void setTitle(String title ){
		final TextView tvTitle = (TextView)findViewById(R.id.abs_top_tv_title);
		tvTitle.setText(title);
	}

}
