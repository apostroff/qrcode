package com.exadel.tests.qrcode_scaner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public final class DataHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "data";
    public static final String TABLE_GOODS = "Goods";
    public static final String TABLE_PAID = "Paid";
    public static final String TABLE_VISA= "Visa";
    public static final int DATABASE_VERSION = 1;



    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

       sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_GOODS+"(_id INTEGER PRIMARY KEY , QR_CODE TEXT , SCANNED_DATE int" +
            ", COMMENT TEXT  , GOOD_NAME TEXT , GOOD_DESC TEXT , PRICE INT , " +
               "QTY INT , GOOD_URL TEXT , SERVER_ID INT , IMAGE BLOB)");
       
       sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_VISA+"(_id INTEGER PRIMARY KEY , CARD_NUMBER TEXT, VALID_YEAR INT,"+
       		"VALID_MONTH INT , OWNER TEXT  , CW2 TEXT , CARD_TYPE INT , LAST_NUMBER TEXT, MONEY_LIMIT INT )");

       sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_PAID+"(_id INTEGER PRIMARY KEY, GOOD_NAME TEXT, GOOD_DESC TEXT ," +
               " PRICE INT, QTY INT, GOOD_URL TEXT ,LAST_NUMBER TEXT , PAID_DATE INT  ,  IMAGE BLOB)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {

    }
}
