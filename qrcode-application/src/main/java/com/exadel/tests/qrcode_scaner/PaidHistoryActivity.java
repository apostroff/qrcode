package com.exadel.tests.qrcode_scaner;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.exadel.tests.qrcode_scaner.adapters.PaidHistoryAdapter;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;

public class PaidHistoryActivity extends AbstractCustomABSActivity implements LoaderCallbacks<Cursor>{

	PaidHistoryAdapter mAdapter;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.alreadypaid_activity);
		mAdapter = new PaidHistoryAdapter(this, null, false);
		getSupportLoaderManager().initLoader(0, null, this);
		
		final ListView lview = (ListView)findViewById(R.id.lv_alreadypaid);
		lview.setAdapter(mAdapter);
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        Uri baseUri = QrContentProvider.CONTENT_URI_PAID;
        return new CursorLoader(PaidHistoryActivity.this, baseUri,QrContentProvider.PAID_COLUMNS, null, null,null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor data) {
		mAdapter.swapCursor(data);
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
		
	}

	public void onClick(View v){
		finish();
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}
}
