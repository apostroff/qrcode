package com.exadel.tests.qrcode_scaner;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.exadel.tests.qrcode_scaner.adapters.GoodsAdapter;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import com.exadel.tests.qrcode_scaner.domain.Goods;

import java.util.ArrayList;

/**
 *  Activity with scanned qr codes history ListView
 *  user can delete or buy using qr code from list 
 * @author apai
 *
 */

public class HistoryActivity extends AbstractCustomABSActivity	implements
        LoaderCallbacks<Cursor> , OnMenuItemClickListener , AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {
	public static final int HISTORY_LOADER_ID = 0;
	public static final String ALL_CHECK = "allcheck"; 
	
	public static final int MENU_DELETE = 0;
	public static final int MENU_BUY = 1;
	public static final int MENU_PAID = 2;
	public static final int MENU_CANCEL = 3;
	
	GoodsAdapter mAdapter;
	private float totalsum=0;
	private TextView tvTotalSum;
    ListView lview;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.history_activity);
		mAdapter = new GoodsAdapter(this, null, false);
		mAdapter.allCheckOnCreate =  getIntent().getBooleanExtra(ALL_CHECK, false);
		
		lview = (ListView)findViewById(R.id.lvHistory);
		lview.setAdapter(mAdapter);
        lview.setOnItemClickListener(this);
        lview.setOnItemLongClickListener(this);
		getSupportLoaderManager().initLoader(0, null, this);
		
		tvTotalSum = (TextView)findViewById(R.id.tv_history_total);
		tvTotalSum.setText(String.valueOf(totalsum));
	}

	

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(Menu.NONE , MENU_DELETE , Menu.NONE ,    R.string.delete)
			.setOnMenuItemClickListener(this)
			.setIcon(R.drawable.ic_remove)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM  );
	
		menu.add(Menu.NONE , MENU_BUY , Menu.NONE ,R.string.buy)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		
		menu.add(Menu.NONE , MENU_PAID , Menu.NONE ,R.string.History)
			.setIcon(R.drawable.ic_clock)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM  );
		
		menu.add(Menu.NONE , MENU_CANCEL , Menu.NONE ,R.string.cancel)
			.setIcon(R.drawable.ic_undo)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );
		
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
	    // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
        Uri baseUri = QrContentProvider.CONTENT_URI_GOODS;


        return new CursorLoader(HistoryActivity.this, baseUri,QrContentProvider.GOODS_COLUMNS, null, null,null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);	
		// if flag all check- all check
		if (getIntent().getBooleanExtra(ALL_CHECK, false)){
			totalsum = getTotalSum();
			tvTotalSum.setText(String.valueOf(totalsum));
		}
			
	
	}
	/**
	 * get sum off all prices
	 * @return
	 */
	private float getTotalSum() {
		Cursor cursor = mAdapter.getCursor();
		if (!cursor.moveToFirst()) return 0f;
		float res = 0f;
		do{
			res+=cursor.getFloat(6)/100f*cursor.getFloat(7);
		}while (cursor.moveToNext());
		return res;
		
	}



	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
	}
	
	/**
	 * on delete hisory record
	 */
	private void onDelete(){
		int del_count=0;
		for (int i=mAdapter.itemChecked.size()-1;i>=0;i--){
			if(mAdapter.itemChecked.get(i)) {
				mAdapter.getCursor().moveToPosition(i);
				Uri delUri = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_GOODS , mAdapter.getCursor().getString(0));
				int count  = getContentResolver().delete(delUri, null, null);
				if (count>0) mAdapter.itemChecked.remove(i);
				del_count+=count;
			}
		}
		totalsum =0;
		tvTotalSum.setText(String.valueOf(totalsum)); 
		Toast.makeText(HistoryActivity.this, "Deleted "+del_count+" rows", Toast.LENGTH_LONG).show();
	}
	
	/***
	 * buy seleced goods- send its to visa screen
	 */
	
	private void onBuy(){
		ArrayList<Goods> goods = new ArrayList<Goods>();
		for (int i=mAdapter.itemChecked.size()-1;i>=0;i--){
			if(mAdapter.itemChecked.get(i)) {
				mAdapter.getCursor().moveToPosition(i);
				goods.add(  new Goods(mAdapter.getCursor()));
			}
		}
		if (goods.size()>0){
			Intent intent = new Intent(HistoryActivity.this , VisaListActivity.class);
			intent.putParcelableArrayListExtra(VisaListActivity.GOODS, goods);
			startActivity(intent);
			overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		}		
	}


	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()){
		case MENU_DELETE : 
			onDelete();
			break;
		case MENU_BUY :
			onBuy();
			break;
		case MENU_PAID :
			Intent intent  = new Intent(this , PaidHistoryActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(intent);
			overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			break;
		case MENU_CANCEL :
			finish();
			overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			break;

		}
		return false;
	}
    /* Click on List view - need to check/uncheck */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long viewid) {
        boolean checked = !mAdapter.itemChecked.get(pos);
        mAdapter.itemChecked.set(pos , checked);
        view.setBackgroundColor(checked?getResources().getColor(R.color.list_view_checked): Color.TRANSPARENT );
        GoodsAdapter.ViewHolder holder = (GoodsAdapter.ViewHolder)view.getTag();
        holder.imChecked.setImageResource(checked?R.drawable.items_check_pressed:R.drawable.items_check );

        Cursor c = mAdapter.getCursor();
        c.moveToPosition(pos);
        totalsum+= c.getFloat(6)/100f*c.getFloat(7)*(checked?1f:-1f);// price column
        tvTotalSum.setText(String.valueOf(totalsum));

    }
    /* Long click on List View item - need to show details of goods */
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long viewid) {

        Cursor c = mAdapter.getCursor();
        c.moveToPosition(pos);


        Intent intent = new Intent();
        intent.setClass(this , DetailsActivity.class);
        intent.putExtra(DetailsActivity.GOODS , c.getInt(0)); // ID

        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        return true;
    }
}

