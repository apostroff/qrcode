package com.exadel.tests.qrcode_scaner;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import com.exadel.tests.qrcode_scaner.dialogs.*;
import com.exadel.tests.qrcode_scaner.domain.Goods;
import com.exadel.tests.qrcode_scaner.scanner.Scanner;

import java.util.ArrayList;



public class MainActivity extends AbstractCustomABSActivity
		implements OnMenuItemClickListener   {
	public static final String FIRST_RUN = "first";
	public static final String SHOW_AGAIN = "ShowAgain";
	
	
	public static final int MENU_LIGHT = 0;
	public static final int MENU_HELP = 1;
	public static final int MENU_SCAN = 2;
	public static final int MENU_VISA = 3;
	public static final int MENU_HISTORY = 4;
	public static final int MENU_SETTINGS = 5;
	
	public static final String TAG = "Main";
	private Menu mMenu;
	private Scanner mScanner;
    private boolean fiststart;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Log.e("onCreate" , this.getClass().getCanonicalName());

        setContentView(R.layout.activity_main);
        mScanner = (Scanner)getSupportFragmentManager().findFragmentById(R.id.scanner);

        mDissmisListener = onDialogDismiss;
	}
	
		@Override
	protected void onSaveInstanceState(Bundle outState){
		outState.putBoolean(FIRST_RUN, false);
		super.onSaveInstanceState(outState);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	this.mMenu = menu;
    	
    	menu.add(Menu.NONE , MENU_LIGHT , Menu.NONE ,    R.string.Light)
    		.setOnMenuItemClickListener(this)
    		.setIcon(R.drawable.ic_lightflash)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS );
    	
    	menu.add(Menu.NONE , MENU_HELP , Menu.NONE ,    R.string.help)
			.setOnMenuItemClickListener(this)
			.setIcon(R.drawable.ic_help)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );
    	
    	
    	menu.add(Menu.NONE , MENU_SCAN , Menu.NONE ,R.string.Pause)
    		.setOnMenuItemClickListener(this)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    	
    	menu.add(Menu.NONE , MENU_VISA , Menu.NONE ,R.string.Visa)
    		.setIcon(R.drawable.ic_visa)
    		.setOnMenuItemClickListener(this)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );
    	
    	menu.add(Menu.NONE , MENU_HISTORY , Menu.NONE ,R.string.History)
			.setIcon(R.drawable.ic_folder)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS );
    	 
        return true;
    }
	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()){
			case MENU_LIGHT : doSomeStufWithlight(); break;			
			case MENU_SCAN : 		// stop/start camera preview
				if (mScanner.getPreviewing()){
					stopCamera();
				}else {
					startCamera();
				}
				break;
				
			case MENU_VISA :
				stopCamera();
				openStoredVisaData();
				break;
			case MENU_HISTORY :
				stopCamera();
				Intent intent = new Intent(MainActivity.this , HistoryActivity.class);
				startActivity(intent);
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
				break;
			case MENU_HELP : 
				stopCamera();
				showHelpDialog();
				break;
		}
		return false;
	}
	OnClickListener onSettingsClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			stopCamera();
			showSettings();
		}
	};
	
	
	/***
	 * show settings url 
	 */
	private void showSettings() {
		PreferenceDialog dlg = new PreferenceDialog();
		dlg.setDismissListener(onDialogDismiss);
		dlg.show(getSupportFragmentManager(), PreferenceDialog.TAG);
//		Intent intent = new Intent(this, PreferenceActivity.class);
//		startActivity(intent);
	}
	
	private void showHelpDialog() {
		HelpDialog dlg = new HelpDialog();
		dlg.setDismissListener(onDialogDismiss);
		dlg.show(getSupportFragmentManager(), HelpDialog.TAG);
		
	}
	private void openStoredVisaData() {
		
		final Cursor cursor = getContentResolver().query(QrContentProvider.CONTENT_URI_VISA,QrContentProvider.VISA_COLUMNS, null, null, null);
		if (!cursor.moveToFirst()){
			AddNewCardDialog dlg = AddNewCardDialog.newInstance();
			dlg.setDismissListener(onDialogDismiss);
			dlg.show(getSupportFragmentManager(), AddNewCardDialog.TAG);
		}else{
			Intent intent = new Intent(this, VisaListActivity.class);
			ArrayList<Goods> allgoods = getAllStoredGoods();
			if (allgoods.size()>0) intent.putParcelableArrayListExtra(VisaListActivity.GOODS, allgoods);
			startActivity(intent);
			overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		}
		
	}

	/****
	 * turn on/off flash light
	 */
	private void doSomeStufWithlight() {

		if (this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
			mScanner.turnOnOffFlash();
		}else
			Toast.makeText(this, R.string.Light_not_available, Toast.LENGTH_LONG).show();
	}

	
	public void stopCamera(){
		if (mMenu!=null)// if menu not attached yet
			mMenu.getItem(MENU_SCAN).setTitle(R.string.Scan);
		mScanner.stopPreview();
	}
	public void startCamera(){
		Log.e("main" , "startCamera");
		if (mMenu!=null)// if menu not attached yet
			mMenu.getItem(MENU_SCAN).setTitle(R.string.Pause);
		mScanner.startPreview();
	}
    
	
	public void onQRScanned(String qrcode){
		mMenu.getItem(MENU_SCAN).setTitle(R.string.Scan);
        // show dialog with scanned 
        QRCodeDialog dlg =  QRCodeDialog.newInstance(qrcode);
        dlg.setDismissListener(onDialogDismiss);
        dlg.show(getSupportFragmentManager(),QRCodeDialog.TAG );
	}

    /*
	@Override
	public void onResume(){
		super.onResume();
		if (fiststart){// if first start
			final Cursor cursor = getContentResolver().query(QrContentProvider.CONTENT_URI_VISA,QrContentProvider.VISA_COLUMNS, null, null, null);
			if (!cursor.moveToFirst()){
				SharedPreferences pref = getSharedPreferences(SHOW_AGAIN, Activity.MODE_PRIVATE);
				if (pref.getBoolean(SHOW_AGAIN, true)){
					stopCamera();
					AskForAddcard dlg = AskForAddcard.newInstance();
					dlg.setDismissListener(onDialogDismiss);
					dlg.show(getSupportFragmentManager(), AskForAddcard.TAG);
				}
			}
			fiststart=false;
		}
		if (mMenu!=null) mMenu.getItem(MENU_SCAN).setTitle(R.string.Pause);  
	}
	@Override
	public void onPause(){
		super.onPause();
		stopCamera();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
               */
    @Override
    public void onSettingsClick() {
        super.onSettingsClick();
        stopCamera();
    }


	private DisFragmentDialog.DismissListener onDialogDismiss = new DisFragmentDialog.DismissListener() {
		@Override
		public void onDismiss() {
			startCamera();
			
		}
	};
	/***
	 * is any fragment dialog active -
	 * need to decision - start or not camera preview
	 * @return
	 */
	
	private boolean isAnyDialogsActive(){
		return getSupportFragmentManager().findFragmentByTag(QRCodeDialog.TAG)!=null ||
				getSupportFragmentManager().findFragmentByTag(HelpDialog.TAG)!=null||
				getSupportFragmentManager().findFragmentByTag(AskForAddcard.TAG)!=null||
				getSupportFragmentManager().findFragmentByTag(PreferenceDialog.TAG)!=null||
				getSupportFragmentManager().findFragmentByTag(AddNewCardDialog.TAG)!=null;
	}
	
	private ArrayList<Goods> getAllStoredGoods() {
		Uri goods_uri = QrContentProvider.CONTENT_URI_GOODS;
		Cursor c = getContentResolver().query(goods_uri, QrContentProvider.GOODS_COLUMNS, null, null, null);
		ArrayList<Goods> res = new ArrayList<Goods>();
		
		if (!c.moveToFirst()) {
			c.close();
			return res;
		}
		do{
			res.add( new Goods(c));
		}while (c.moveToNext());
		c.close();
		return res;
	}
}
