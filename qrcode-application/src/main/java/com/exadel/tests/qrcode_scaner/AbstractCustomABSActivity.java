package com.exadel.tests.qrcode_scaner;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.exadel.tests.qrcode_scaner.dialogs.DisFragmentDialog;
import com.exadel.tests.qrcode_scaner.dialogs.PreferenceDialog;

/**
 * Created with IntelliJ IDEA.
 * User: apai
 * Date: 26.02.13
 * Time: 14:02
 * used as template to each activity
 */
public class AbstractCustomABSActivity extends SherlockFragmentActivity implements ActionBarCustomTitle.CustomTitleListener  {
    /** listener to dismiss preference dialog - used on mainactivity */
    DisFragmentDialog.DismissListener mDissmisListener;
    @Override
    protected void onCreate(Bundle saveInstance){
        super.onCreate(saveInstance);

        final  ActionBarCustomTitle ctitle = new ActionBarCustomTitle(this );
        FrameLayout.LayoutParams prms = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER_VERTICAL);
        ctitle.setLayoutParams(prms);
        ctitle.listener=this;
        getSupportActionBar().setCustomView(ctitle);
    }
    /***
     * show animation on back
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }


    @Override
    public void onHomeClick() {
        onBackPressed();
    }

    @Override
    public void onSettingsClick() {
        PreferenceDialog dlg = new PreferenceDialog();
        dlg.setDismissListener(mDissmisListener);
        dlg.show(getSupportFragmentManager(), PreferenceDialog.TAG);
    }
}
