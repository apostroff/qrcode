package com.exadel.tests.qrcode_scaner.domain;



public class PayCard {

	
	public static final int TYPE_VISA=0;
	public static final int TYPE_MAESTRO=1;
	public static final int TYPE_MASTERCARD=2;
	public static final String PAYPAL = "paypal";
	
	public String number;
	public int type;
	public int cw2;
	public int valid_year;
	public int valid_month;
	public String owner;
	
	
	public PayCard(int type , String number , int val_year , int val_month , int cw2 ){
		
		this.type = type;
		this.number= number;
		this.valid_month = val_month;
		this.valid_year = val_year;
		this.cw2 = cw2;
	}
	
	public PayCard(int type , String number , int val_year , int val_month , int cw2  , String own){
		this(type , number , val_year , val_month , cw2);
		this.owner = own;
	}
}
