package com.exadel.tests.qrcode_scaner.scanner;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.view.View;
import com.exadel.tests.qrcode_scaner.R;
/*****
 *  View displaying overlay text and borders on camera preview
 * @author apai
 *
 */
public class OverlayView extends View {

	private int mScreenHeight , mScreenWidth;
	private Paint mPaint;
	private String mOverlayText;
	private int mTextSize;
	// border parameters 
	private int margin_left;
	private int margin_top;
	private int margin_bottom;
	private int border_width;
	private int border_height;
	private int border_thickness;
    private Bitmap mBtFrame;
    private Bitmap mMessageBitmap;


	
	public OverlayView(Context context) {
		super(context);
		mPaint = new Paint();
        mPaint.setARGB(255 , 234,234,234);
		mPaint.setAntiAlias(true);
		mPaint.setTypeface(Typeface.DEFAULT_BOLD);
		
		mOverlayText = getResources().getString(R.string.overlay_text);
		// border 
		margin_left= (int) getResources().getDimension(R.dimen.ol_margin_left);
		margin_top= (int) getResources().getDimension(R.dimen.ol_margin_top);
		margin_bottom= (int) getResources().getDimension(R.dimen.ol_margin_bottom);
		border_width= (int) getResources().getDimension(R.dimen.ol_width);
		border_height= (int) getResources().getDimension(R.dimen.ol_height);
		border_thickness= (int) getResources().getDimension(R.dimen.ol_thickness);
		mPaint.setStrokeWidth(border_thickness);

        mBtFrame = BitmapFactory.decodeResource(getResources(),R.drawable.frame );
        mMessageBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.focus_message );

	}
	
	public OverlayView(Context context, AttributeSet attrs) {
		this(context);
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mScreenHeight==0||mScreenWidth==0) return;
		// text
        /*
		canvas.drawText(mOverlayText, 
				mScreenWidth/2-mPaint.measureText(mOverlayText)/2, 
				mScreenHeight-mPaint.getTextSize(), mPaint);
          */
        drawWhiteFrameFromResource(canvas);
        drawFrame(canvas);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		mScreenHeight = h;
		mScreenWidth = w;
		if (w==0||h==0) return;

		mTextSize = Math.min(h, w)/12;
		mPaint.setTextSize(mTextSize);



	}

    /****
     * draw frame by lines color - red
     * @param canvas
     */
    private void drawRedFrame(Canvas canvas){
        canvas.drawLine(margin_left+border_thickness/2,
                margin_top,
                margin_left+border_thickness/2,
                margin_top+border_height, mPaint);// | vertical
        canvas.drawLine(margin_left,
                margin_top,
                margin_left+border_width,
                margin_top, mPaint);// - horizontal
        // border bottom
        canvas.drawLine(mScreenWidth-margin_left,
                mScreenHeight-margin_bottom-border_height,
                mScreenWidth-margin_left,
                mScreenHeight-margin_bottom, mPaint);// | vertical
        canvas.drawLine(mScreenWidth-margin_left-border_width+border_thickness/2,
                mScreenHeight-margin_bottom,
                mScreenWidth-margin_left+border_thickness/2,
                mScreenHeight-margin_bottom, mPaint);// -horizontal
    }

    /***
     * draw frame from resources (Bitmap)
     * @param canvas
     */

    private void drawWhiteFrameFromResource(Canvas canvas){
        final int centerX = mScreenWidth/2;
        final int centerY = (mScreenHeight-margin_bottom+margin_top) /2;
        canvas.drawBitmap(mBtFrame , centerX-mBtFrame.getWidth()/2 , centerY-mBtFrame.getHeight()/2  , null );
    }

    private void drawFrame(Canvas canvas){
        final int centerX = mScreenWidth/2;
        final int centerY = mScreenHeight-margin_bottom/2;
        canvas.drawBitmap(mMessageBitmap ,centerX-mMessageBitmap.getWidth()/2 , centerY-mMessageBitmap.getHeight()/2  , null);

    }



}
