package com.exadel.tests.qrcode_scaner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.util.ArrayList;

public class TabsAdapter extends FragmentPagerAdapter implements ActionBar.TabListener, ViewPager.OnPageChangeListener{
	public static final String TAG = "TabsAdapter"; 
	private final Context mContext;
	private final ActionBar mActionBar;
	private final ViewPager mViewPager;
	private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
	private TabSelectListener mOnTabSelect = null;
	private final ArrayList<Fragment> mFragments = new ArrayList<Fragment>(); 
	
	public void setTabSelectListener(TabSelectListener listener){
		mOnTabSelect = listener;
	}
	
	public static interface TabSelectListener{
		public void onTabSelected(int tabNo);
		public void onSwapping(int position, float positionOffset,int positionOffsetPixels);
	}
	
	static final class TabInfo{
		private final Class<?> clss;
		private final Bundle args;

		TabInfo(Class<?> _class, Bundle _args){
		        clss = _class;
		        args = _args;
		}
	}

	public TabsAdapter(SherlockFragmentActivity activity, ViewPager pager){
		super(activity.getSupportFragmentManager());
		mContext = activity;
		mActionBar = activity.getSupportActionBar();
		mViewPager = pager;
		mViewPager.setAdapter(this);
		mViewPager.setOnPageChangeListener(this);
	}

	public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
		TabInfo info = new TabInfo(clss, args);
		tab.setTag(info);
		tab.setTabListener(this);
		mTabs.add(info);
		mActionBar.addTab(tab);
		notifyDataSetChanged();
	}

	@Override
	public int getCount(){
		return mTabs.size();
	}

	@Override
	public Fragment getItem(int position){
		TabInfo info = mTabs.get(position);
		if (mFragments.size()>position)
			return mFragments.get(position);
		else {
			mFragments.add(SherlockFragment.instantiate(mContext, info.clss.getName(),info.args));
			return mFragments.get(mFragments.size()-1); 		
		}			
		
		
	}

	public void onPageScrolled(int position, float positionOffset,int positionOffsetPixels)	{
		
		if (mOnTabSelect != null) mOnTabSelect.onSwapping(position, positionOffset,positionOffsetPixels);
	}
	
	public void onPageSelected(int position){
		mActionBar.setSelectedNavigationItem(position);
		if (mOnTabSelect != null) mOnTabSelect.onTabSelected(position);
	}
	
	public void onPageScrollStateChanged(int state)	{

	}
	
	public void onTabSelected(Tab tab, FragmentTransaction ft)	{
		Object tag = tab.getTag();
		int i;
		for (i = 0; i < mTabs.size(); i++){
		        if (mTabs.get(i) == tag){
		                mViewPager.setCurrentItem(i);
		                break;
		        }
		}
	//	if (mOnTabSelect != null) mOnTabSelect.onTabSelected(i);
	}
	
	public void onTabUnselected(Tab tab, FragmentTransaction ft){
		
	}
	
	public void onTabReselected(Tab tab, FragmentTransaction ft){
	}

}
