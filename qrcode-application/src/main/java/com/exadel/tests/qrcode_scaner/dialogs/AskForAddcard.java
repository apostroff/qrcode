package com.exadel.tests.qrcode_scaner.dialogs;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import com.exadel.tests.qrcode_scaner.MainActivity;
import com.exadel.tests.qrcode_scaner.R;
/**
 * Dialog ask for add new payment card , if no one card found
 * its shown when app started
 * @author apai
 *
 */
public class AskForAddcard extends DisFragmentDialog {
	public static final String TAG = "askdlg";
	
	/***
	 * factory method for create new dialog
	 * @param showagain
	 * @return
	 */
	public static AskForAddcard newInstance(){		
		AskForAddcard dlg = new AskForAddcard();
		return dlg;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int style = DialogFragment.STYLE_NO_TITLE;
		setStyle(style,0);
 
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.promt_for_add_dialog, container, false);
		final  Button btAdd = (Button)v.findViewById(R.id.bt_Add);
		btAdd.setOnClickListener(onAdd);
		
		final Button btCancel = (Button)v.findViewById(R.id.bt_cancel);
		btCancel.setOnClickListener(onCancel);
		
		final CheckBox cbShowAskAgain = (CheckBox)v.findViewById(R.id.cbShowAgain);
		cbShowAskAgain.setChecked(true);
		cbShowAskAgain.setOnClickListener(onCheckAskAgain);
		return v;
	}
	
	
	OnClickListener onAdd = new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
			AddNewCardDialog dlg = AddNewCardDialog.newInstance();
			dlg.show(getActivity().getSupportFragmentManager(), AddNewCardDialog.TAG);
		}
	};
	
	
	OnClickListener onCancel = new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
		}
	};
	
	/**
	 * save preferance
	 */
	OnClickListener onCheckAskAgain = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			SharedPreferences pref = getActivity().getSharedPreferences(MainActivity.SHOW_AGAIN, Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor= pref.edit();
			editor.putBoolean(MainActivity.SHOW_AGAIN, ((CheckBox)v).isChecked());
			editor.commit();
		}
	};
	
}
