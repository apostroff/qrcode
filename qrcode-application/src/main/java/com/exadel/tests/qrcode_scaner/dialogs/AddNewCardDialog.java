package com.exadel.tests.qrcode_scaner.dialogs;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import com.exadel.tests.qrcode_scaner.domain.PayCard;
import com.exadel.tests.qrcode_scaner.secure.Crypt;

public class AddNewCardDialog extends DisFragmentDialog {
	public static final String TAG = "ADDCARD";
	TextView edNumber , edYear , edMonth , edOwner , edCw2 , edLimit ;
	RadioButton rb_visa , rb_master , rb_maestro;
	Button btSave , btCancel;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	     int style = DialogFragment.STYLE_NORMAL, theme = 0;
	     this.setRetainInstance(true);
	     
	     setStyle(style,theme);
	}
	/**
	 * fabric method for create new card
	 * @return
	 */
	public static  AddNewCardDialog newInstance(){
		return new AddNewCardDialog();
	}
	/**
	 * Fabric method for edit exist card properties
	 * @param type
	 * @param number
	 * @param month
	 * @param year
	 * @param cw2
	 * @param owner
	 * @param limit
	 */
	public static AddNewCardDialog newInstance(int id, int type , String number ,  int month , int year , String cw2 , String owner , int limit  ){
		Log.e(TAG , "id="+id+"  number="+number+" type = "+type);
		AddNewCardDialog dlg = new AddNewCardDialog();
		Bundle args = new Bundle();
		args.putInt(QrContentProvider.VISA_ID, id);
		args.putString(QrContentProvider.VISA_NUMBER, number);
		args.putString(QrContentProvider.VISA_MONTH, String.valueOf(month));
		args.putString(QrContentProvider.VISA_YEAR, String.valueOf(year));
		args.putInt(QrContentProvider.VISA_TYPE, type);
		args.putString(QrContentProvider.VISA_CW2, cw2);
		args.putString(QrContentProvider.VISA_OWNER, owner);
		args.putString(QrContentProvider.VISA_LIMIT, String.valueOf(limit));
		dlg.setArguments(args);
		return dlg;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.new_card_layout, container, false);
		getDialog().setTitle(R.string.addcard_title);
		loadContent(v );
		Bundle args = getArguments();
		if (getCardID()!=-1) 	setContent(args);
		getDialog().setCanceledOnTouchOutside(false);

		return v;
	}
	
	private void setContent(Bundle args) {
		edNumber.setText(args.getString(QrContentProvider.VISA_NUMBER));
		edYear.setText(args.getString(QrContentProvider.VISA_YEAR));
		edMonth.setText(args.getString(QrContentProvider.VISA_MONTH));
		edOwner.setText(args.getString(QrContentProvider.VISA_OWNER));
		edCw2.setText(args.getString(QrContentProvider.VISA_CW2));
		edLimit.setText(args.getString(QrContentProvider.VISA_LIMIT));
		setCardType(args.getInt(QrContentProvider.VISA_TYPE));
	}
	private void loadContent(View v ){
		edNumber = (TextView)v.findViewById(R.id.ed_card_number); 
		edYear = (TextView)v.findViewById(R.id.ed_year);
		edMonth= (TextView)v.findViewById(R.id.ed_month);
		edOwner= (TextView)v.findViewById(R.id.ed_owner);
		edCw2 = (TextView)v.findViewById(R.id.ed_cw2);
		edLimit = (TextView)v.findViewById(R.id.ed_limit);
				
		btSave = (Button)v.findViewById(R.id.bt_save);
		btSave.setOnClickListener(onSave);
		btCancel = (Button)v.findViewById(R.id.bt_cancel);
		btCancel.setOnClickListener(onCancel);
		
		rb_visa = (RadioButton)v.findViewById(R.id.rb_visa); 
		rb_master= (RadioButton)v.findViewById(R.id.rb_masterCard); 
		rb_maestro= (RadioButton)v.findViewById(R.id.rb_maestro);
	}
	
	OnClickListener onCancel = new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
		}
	};
	
	OnClickListener onSave = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			save_card();
			
		}
	};
	
	public void save_card(){
		PayCard card = test_valid();
		if (card==null) return ;
		LockPatternDialog dlg = LockPatternDialog.newInstance();
		dlg.show(getActivity().getSupportFragmentManager(), LockPatternDialog.TAG);
	}
	/***
	 * check validity entered data
	 * @return new PayCard if all right  , in otherwise return null 
	 */
	
	public PayCard test_valid(){
		String month = edMonth.getText().toString();
		int imonth;
		try{
			imonth = Integer.decode(month);
			if (imonth>12||imonth==0) {
				Toast.makeText(getActivity(), "Invalid month", Toast.LENGTH_LONG).show();
				return null; 
			}
		}catch(NumberFormatException e){
			Toast.makeText(getActivity(), "Invalid month", Toast.LENGTH_LONG).show();
			return null;
		}
		
		String year = edMonth.getText().toString();
		int iyear;
		try{
			iyear = Integer.decode(year);
		}catch(NumberFormatException e){
			Toast.makeText(getActivity(), "Invalid year", Toast.LENGTH_LONG).show();
			return null;
		}
		String snumber = edNumber.getText().toString();
		if (snumber.length()!=16){
			Toast.makeText(getActivity(), "Invalid number", Toast.LENGTH_LONG).show();
			return null;
		}
        String ownerName = edOwner.getText().toString();
        if (ownerName.length()==0){
            Toast.makeText(getActivity(), "Owner information is required", Toast.LENGTH_LONG).show();
            return null;
        }

		String scw2 = edCw2.getText().toString();
		if (scw2.length()!=3) {
			Toast.makeText(getActivity(), "Invalid CW2", Toast.LENGTH_LONG).show();
			return null;
		}
		int cw2 = Integer.decode(scw2);
		
		int type = getCardType();

		return new PayCard(type,snumber, iyear, imonth, cw2 , ownerName);
	}
	private int getCardType() {
		if (rb_visa.isChecked()) return PayCard.TYPE_VISA;
		if (rb_maestro.isChecked()) return PayCard.TYPE_MAESTRO;
		if (rb_master.isChecked()) return PayCard.TYPE_MASTERCARD;
		return 0;
	}
	private void setCardType(int cardType){
		switch (cardType){
			case PayCard.TYPE_VISA : 
				rb_visa.setChecked(true);
				break;
			case PayCard.TYPE_MASTERCARD :
				rb_master.setChecked(true);
				break;
			case PayCard.TYPE_MAESTRO :
				rb_maestro.setChecked(true);
				break;
				
		}
	}
	/***
	 * user set password on this card
	 * so we need to encode data and store to database
	 * @param sha1_key
	 */
	public void onSetPassword(String sha1_key){
		dismiss();
		String number =edNumber.getText().toString();
		String encode_card_number	= Crypt.encrypt(number, sha1_key);
		
		ContentValues val = new ContentValues();
		val.put(QrContentProvider.VISA_NUMBER, encode_card_number);
		val.put(QrContentProvider.VISA_MONTH, edMonth.getText().toString());
		val.put(QrContentProvider.VISA_YEAR, edYear.getText().toString());
		val.put(QrContentProvider.VISA_LAST_NUMBER, number.substring(12,16));
		val.put(QrContentProvider.VISA_TYPE, getCardType());
        val.put(QrContentProvider.VISA_OWNER, edOwner.getText().toString());
		val.put(QrContentProvider.VISA_CW2, Crypt.encrypt(edCw2.getText().toString(), sha1_key));		
		val.put(QrContentProvider.VISA_LIMIT, getLimit() );
		
		
		ContentResolver resolver = getActivity().getContentResolver();
		int cardID = getCardID();
		if (cardID==-1){ // its new card - just insert it 
			try{
				resolver.insert(QrContentProvider.CONTENT_URI_VISA, val);
			}catch(SQLException e){
				Toast.makeText(getActivity(), "Cant save card :"+e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
				return ;
			}
		}else{ // update card with this id 
			try{
				Uri urlupdate  =  Uri.withAppendedPath(QrContentProvider.CONTENT_URI_VISA , String.valueOf(cardID));
				resolver.update(urlupdate, val, null, null);
			}catch(IllegalArgumentException e){
				Toast.makeText(getActivity(), "Cant save card :"+e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
				return ;
			}
			
		}
		
	}


	
	private int getLimit(){
		try{
			return Integer.decode(edLimit.getText().toString());
		}catch(NumberFormatException e){
			return 0;
		}
	}
	/**
	 * returns card ID from save bundle or -1 if its new payment card
	 * @return
	 */
	private int getCardID(){
		Bundle args = getArguments();
		
		if (args==null||!args.containsKey(QrContentProvider.VISA_ID)) 	return -1;
		return args.getInt(QrContentProvider.VISA_ID);
	}
}
