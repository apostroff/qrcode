package com.exadel.tests.qrcode_scaner;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import com.exadel.tests.qrcode_scaner.dialogs.QRCodeDialog;
import com.exadel.tests.qrcode_scaner.domain.Goods;

import java.util.ArrayList;

/**
  * User: apai
 * Date: 11.03.13
 * Time: 14:17
 *  Activity used to show details of good
 */
public class DetailsActivity extends  AbstractCustomABSActivity
    implements MenuItem.OnMenuItemClickListener {
    public static final String GOODS = "Goods";
    public static final int MENU_DELETE = 0;
    public static final int MENU_BUY = 1;
    public static final int MENU_HISTORY=2;
    public static final int MENU_CANCEL = 3;
    Goods goods =null;
    EditText edQty;
    @Override
    protected void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);


        final int goodID  = getIntent().getIntExtra(GOODS, -1);
        goods = Goods.getGoodsByID(goodID , getApplicationContext());
        if (goods==null){
            Toast.makeText(getApplicationContext() , "no goods" , Toast.LENGTH_LONG).show();
            return;
        }
        setContentView(R.layout.details_layout);

        final ImageView imView = (ImageView)findViewById(R.id.im_detailsgood_image);
        imView.setImageBitmap(goods.imageBitmap);
        imView.post(new Runnable() {
            @Override
            public void run() {
                imView.setLayoutParams(new android.widget.FrameLayout.LayoutParams(imView.getMeasuredWidth(), imView.getMeasuredWidth()));
            }
        });


        final TextView tvName = (TextView)findViewById(R.id.tv_detailsgood_name);
        tvName.setText(goods.name);
        final TextView tvDesc = (TextView)findViewById(R.id.tv_detailsgood_desc);
        tvDesc.setText(goods.desc);

        final TextView tvDate = (TextView)findViewById(R.id.tv_detailsgood_date);
        tvDate.setText(DateFormat.format(QRCodeDialog.DATE_FORMAT, goods.idate).toString() );

        final TextView tvPrice = (TextView)findViewById(R.id.tv_detailsgood_price);
        tvPrice.setText(String.valueOf(goods.getPrice()));

        edQty = (EditText)findViewById(R.id.ed_detailsgood_qty);
        edQty.setText(String.valueOf(goods.qty));

        final EditText edComment = (EditText)findViewById(R.id.ed_detailsgood_comment);
        edComment.setText(goods.comment);

        final TextView tvUrl =  (TextView)findViewById(R.id.tv_detailsgood_url);
        // set underline style to textview
        SpannableString content = new SpannableString(goods.detailUrl);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvUrl.setText(content);
        tvUrl.setOnClickListener(onUrlClick);

        findViewById(R.id.bt_detailsgood_plus).setOnClickListener(onPlus);
        findViewById(R.id.bt_detailsgood_minus).setOnClickListener(onMinus);

    }
    /***
     * start browser after click on url textview
     */
    View.OnClickListener onUrlClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = ((TextView)v).getText().toString();
            if (url.length()==0) return;
            if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;

            Intent intent =new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        menu.add(Menu.NONE , MENU_DELETE, Menu.NONE ,R.string.delete)
                .setIcon(R.drawable.ic_remove)
                .setOnMenuItemClickListener(this)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |  MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        menu.add(Menu.NONE , MENU_HISTORY, Menu.NONE ,R.string.delete)
                .setIcon(R.drawable.ic_clock)
                .setOnMenuItemClickListener(this)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |  MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        menu.add(Menu.NONE, MENU_BUY, Menu.NONE, R.string.pay)
                .setOnMenuItemClickListener(this)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        menu.add(Menu.NONE , MENU_CANCEL, Menu.NONE ,R.string.cancel)
                .setIcon(R.drawable.ic_undo)
                .setOnMenuItemClickListener(this)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case MENU_CANCEL : onBackPressed();break;
        }
        return false;
    }

    private View.OnClickListener onPlus  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int newQty = getQtyFromEditText()+1;
            edQty.setText(String.valueOf(newQty));
        }
    };

    private View.OnClickListener onMinus = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int newQty = Math.max(getQtyFromEditText()-1,1);
            edQty.setText(String.valueOf(newQty));
        }
    };


    /**
     * set qty to record from edit box in form
     * @return
     */
    private int getQtyFromEditText(){
        int qty;
        try {
            qty =  Integer.decode(edQty.getText().toString());
        }catch(NumberFormatException e){
            qty=1;
        }
        return qty;
    }
}
