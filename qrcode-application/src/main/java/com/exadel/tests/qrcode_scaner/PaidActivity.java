package com.exadel.tests.qrcode_scaner;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.exadel.tests.qrcode_scaner.adapters.GoodsAdapter;
import com.exadel.tests.qrcode_scaner.dialogs.QRCodeDialog;
import com.exadel.tests.qrcode_scaner.domain.Goods;

import java.util.ArrayList;

/****
 * Screen for display checkout goods details
 * @author apai
 *
 */
public class PaidActivity extends AbstractCustomABSActivity {
	public static final String PAIDED = "paid";  
	public static final String TRANS_ID = "transid";
	ArrayList<Goods> paid_goods;
	GoodAdapter mAdapter;
	TextView tv_totalSum ;
	
	@Override
	public void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		if ((paid_goods=getIntent().getParcelableArrayListExtra(VisaListActivity.GOODS))==null) return ;
		setContentView(R.layout.paided_screen);
		if (getIntent().getBooleanExtra(PAIDED, false)) {
			loadTransactionID();
		}else {// hide image and trans id 
			findViewById(R.id.im_paided).setVisibility(View.GONE);
			findViewById(R.id.tv_paided_trans).setVisibility(View.GONE);
			findViewById(R.id.tv_trans_id).setVisibility(View.GONE);
			findViewById(R.id.tv_paid_thanks).setVisibility(View.GONE);

		}
		
		mAdapter = new GoodAdapter(this, R.layout.good_layout, paid_goods);
		final ListView list = (ListView)findViewById(R.id.lv_paided_goods);
		list.setAdapter(mAdapter);
	
		setTotalSum();
		
	}
	/**
	 * set paid transaction ID to UI textview 
	 */
	private void loadTransactionID() {
		final TextView tv_trans_id = (TextView)findViewById(R.id.tv_paided_trans);
		if (tv_trans_id!=null){
			String transID = getIntent().getStringExtra(TRANS_ID);
			if (transID==null) transID = "";
			tv_trans_id.setText(transID);
		}
		
	}

	private void setTotalSum() {
		tv_totalSum = (TextView)findViewById(R.id.tv_paided_total);
		float sum=0;
		for (Goods row : paid_goods){
			sum+=row.getSum();
		}
		tv_totalSum.setText(String.valueOf(sum));
		
	}
	/**
	 * simple array adapter for goods list
	 * @author apai
	 *
	 */
	public class GoodAdapter extends ArrayAdapter<Goods>{
	    public GoodAdapter(Context context, int textViewResourceId, ArrayList<Goods> items) {
            super(context, textViewResourceId, items);
	    }
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	//	super.getView(position, convertView, parent);
            View v = convertView;
            if (v == null) {// apply viewholder pattern
                LayoutInflater vi = (LayoutInflater)PaidActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.good_layout, null);
                GoodsAdapter.ViewHolder holder = new GoodsAdapter.ViewHolder();
                holder.tvName = (TextView) v.findViewById(R.id.tv_qr_row_name);


                holder.tvQty = (TextView) v.findViewById(R.id.tv_qr_row_qty);
                holder.tvSum = (TextView) v.findViewById(R.id.tv_qr_row_sum);

                holder.tvDate		= (TextView) v.findViewById(R.id.tvHistoryRowDate);
                v.setTag(holder);
            }
            GoodsAdapter.ViewHolder holder = (GoodsAdapter.ViewHolder)v.getTag();
            // set data to UI controls
            Goods record = paid_goods.get(position);  
    		holder.tvName.setText(record.name);


            holder.tvQty.setText(String.valueOf(record.qty));
            holder.tvSum.setText(String.valueOf(record.getSum()));

    	    holder.tvDate.setText(DateFormat.format(QRCodeDialog.DATE_FORMAT,record.idate).toString()  );
            return v;
	            
	    }
	}
	/**
	 * open browser with url of good
	 */
	OnClickListener onUrlClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String url = ((TextView)v).getText().toString();
			if (url.length()==0) return;
			if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
			
			Intent intent =new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			PaidActivity.this.startActivity(intent);
		}
	};  
	/***
	 * finish paid screen and go to scanner screen if goods is paid
	 * @param v  View handing click
	 */
	public void onClick(View v){
		showMainAndClose();
	}
	@Override
	public void  onBackPressed(){
		showMainAndClose();
	}
	private void showMainAndClose(){
		if (getIntent().getBooleanExtra(PAIDED, false)){
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}	
		finish();
	}
}
