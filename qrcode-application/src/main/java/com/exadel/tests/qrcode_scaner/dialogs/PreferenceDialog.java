package com.exadel.tests.qrcode_scaner.dialogs;


import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.exadel.tests.qrcode_scaner.MainActivity;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.adapters.VisaAdapter;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;

/****
 * dialog for save url payment confirm to the preference
 * @author apai
 *
 */
public class PreferenceDialog extends DisFragmentDialog
        implements LoaderManager.LoaderCallbacks<Cursor>,AdapterView.OnItemClickListener {
	public static final String TAG="PRD"; 
	public static final String PREF_URL = "url";
	private EditText mEditUrl;

    ListView mListView     ;
    VisaAdapter mAdapter;
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int style = DialogFragment.STYLE_NO_TITLE;
		setStyle(style,R.style.MyDialogTheme);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.preferance_dialog, container , false);
		mEditUrl = (EditText)v.findViewById(R.id.ed_prefurl);
		loadPref();
		
		final Button btOK = (Button)v.findViewById(R.id.bt_ok);
		btOK.setOnClickListener(onSaveUrl);
		
		final Button btCancel = (Button)v.findViewById(R.id.bt_cancel);
		btCancel.setOnClickListener(onCancel);
        prepareListView(v);
		return v;
	}
	OnClickListener onCancel = new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
		}
	}; 
	
	OnClickListener onSaveUrl = new OnClickListener() {
		@Override
		public void onClick(View v) {
			savePref(mEditUrl.getText().toString());
			dismiss();
		}
	};
	private void savePref(String strurl){
		SharedPreferences pref= getActivity().getSharedPreferences(MainActivity.SHOW_AGAIN, Activity.MODE_PRIVATE);
		Editor edit = pref.edit();
		edit.putString(PREF_URL, strurl);
        if (mAdapter.getSelected()>-1)    {
            mAdapter.getCursor().moveToPosition(mAdapter.getSelected())   ;
            edit.putInt(VisaAdapter.VISA_DEFAULT_PREF , mAdapter.getCursor().getInt(0));
        }
		edit.commit();
	}
	
	private void loadPref(){
		SharedPreferences pref= getActivity().getSharedPreferences(MainActivity.SHOW_AGAIN, Activity.MODE_PRIVATE);
		mEditUrl.setText(pref.getString(PREF_URL, ""));
}

    private void prepareListView(View container){
        mListView  = (ListView)container.findViewById(R.id.lv_pref_visas);
        mAdapter = new VisaAdapter(getActivity(), null, false);
        //tvHint = (TextView)findViewById(R.id.tv_visas_hint);
        mListView.setAdapter(mAdapter);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);

        mListView.setOnItemClickListener(this);
        mListView.setClickable(true);
        mListView.setSelected(true);
        mListView.setEnabled(true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Uri baseUri = QrContentProvider.CONTENT_URI_VISA;
        return new CursorLoader(getActivity(), baseUri,QrContentProvider.VISA_COLUMNS, null, null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long viewid) {
        mAdapter.setSelected(pos, view);
    }
}
