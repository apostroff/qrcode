package com.exadel.tests.qrcode_scaner.domain;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;


/***
 *  entity for scanned qr code
 *  
 * @author apai
 *
 */

public class Goods implements Parcelable{
	public String restService ;
	public String qrcode;
	public long idate;
	public String comment;
	public int qty=1;
    public int server_id;
	public String name="";
	public String desc="";
	public String detailUrl="";
    public String imageName="";
    public Bitmap imageBitmap;

    private float price;


    public float getPrice() {
        return price;
    }

	public Integer id;

	
	public Goods(String qr , long _date , String _comment) throws JSONException{
		qrcode  = qr;
		idate = _date;
		comment = _comment;
		parseFromJson(qr);
	}

    public void parseFromJson(String jsonString) throws JSONException  {

        JSONObject jsobject  = new JSONObject(jsonString);
        name = jsobject.getString("name");
        desc = jsobject.getString("desc");
        price= (float)jsobject.getDouble("price");
        detailUrl = jsobject.getString("url");
        server_id = jsobject.getInt("id");
        imageName = jsobject.getString("picture");
    }
    public JSONObject getJSONObjectForPost() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("goodid",server_id);
            obj.put("qty",qty);
            obj.put("discount",0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

	public Goods(Cursor cursor) {

		try{
			id = cursor.getInt(0);
			qrcode = cursor.getString(1);
			comment = cursor.getString(2);
			idate = cursor.getLong(3);
			name = cursor.getString(4);
			desc = cursor.getString(5);
			price = (float)cursor.getInt(6)/100;
            qty  = cursor.getInt(7);
            detailUrl = cursor.getString(8);
            server_id  = cursor.getInt(9);
            byte[] imageData = cursor.getBlob(10);
            imageBitmap = BitmapFactory.decodeByteArray(imageData , 0 , imageData.length);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
    public float getSum(){
        return (float)qty*price;
    }
    /**
     *  When scanned qr code for
     * @param jsonString
     * @throws JSONException
     */
	public void loadFromJson(String jsonString) throws JSONException  {
		JSONObject jsobject  = new JSONObject(jsonString);
		server_id = jsobject.getInt("id");
		restService =  jsobject.getString("url");
		
	}

	@Override
	public int describeContents() {
		
		return 0;
	}
	/****
	 * Create bundle for Parcelable
	 * @return
	 */
	public Bundle getBunlde(){
		Bundle bundle  = new Bundle();
		
		if (id!=null) bundle.putInt(QrContentProvider.GOODS_ID, id);
		bundle.putString(QrContentProvider.GOODS_QR, qrcode);
		bundle.putString(QrContentProvider.GOODS_COMMENT, comment);
		bundle.putLong(QrContentProvider.GOODS_SCANNED_DATE, idate);
		bundle.putString(QrContentProvider.GOODS_NAME, name);
		bundle.putString(QrContentProvider.GOODS_DESC, desc);
		bundle.putFloat(QrContentProvider.GOODS_PRICE, price);
        bundle.putInt(QrContentProvider.GOODS_QTY, qty);
        bundle.putInt(QrContentProvider.GOODS_SERVER_ID, server_id);
		bundle.putString(QrContentProvider.GOODS_URL, detailUrl);
     //   bundle.putParcelable(QrContentProvider.GOODS_IMAGE , imageBitmap);

		return bundle;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeBundle(getBunlde());

	}

     public static final Parcelable.Creator<Goods> CREATOR = new Parcelable.Creator<Goods>() {
         public Goods createFromParcel(Parcel in) {
             return new Goods(in.readBundle());
         }

         public Goods[] newArray(int size) {
             return new Goods[size];
         }
     };
     
    private Goods(Bundle bundle) {

        if  (bundle.containsKey(QrContentProvider.GOODS_ID))
		    id = bundle.getInt(QrContentProvider.GOODS_ID);
		qrcode = bundle.getString(QrContentProvider.GOODS_QR);
		comment = bundle.getString(QrContentProvider.GOODS_COMMENT);
		idate = bundle.getLong(QrContentProvider.GOODS_SCANNED_DATE);
		name = bundle.getString(QrContentProvider.GOODS_NAME);
		desc = bundle.getString(QrContentProvider.GOODS_DESC);
		price = bundle.getFloat(QrContentProvider.GOODS_PRICE);
        qty =  bundle.getInt(QrContentProvider.GOODS_QTY);
        server_id =  bundle.getInt(QrContentProvider.GOODS_SERVER_ID);
		detailUrl = bundle.getString(QrContentProvider.GOODS_URL);
        //imageBitmap = bundle.getParcelable(QrContentProvider.GOODS_IMAGE);

     }



    public Uri save(Context ctx) throws  IllegalArgumentException {
        if (id==null) return  insert(ctx);
        else return update(ctx);
    }

    /**
     * insert new record in database
     * @param ctx  context
     * @return  inserted Uri
     */
    private Uri insert(Context ctx){
        if (isSuchRecord(ctx)) throw new IllegalArgumentException();
        ContentResolver resolver = 	ctx.getContentResolver();
        try{
            Uri res = resolver.insert(QrContentProvider.CONTENT_URI_GOODS, getContentValues());
            id= Integer.decode(res.getLastPathSegment());
            return res;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }catch(NumberFormatException e){
            e.printStackTrace();
            return null;
        }
    }
    private Uri update(Context ctx){
        ContentResolver resolver = 	ctx.getContentResolver();

        Uri result = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_GOODS , String.valueOf(id));
        try{
            resolver.update(result , getContentValues() , null , null);
            return result;
        }catch(NullPointerException e){
            e.printStackTrace();
            return null;
        }catch(IllegalArgumentException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * set goods data into ContectValues for database operations like insert or update
     * @return  ContentValues
     */
    private   ContentValues getContentValues(){
        ContentValues val = new ContentValues();
        val.put(QrContentProvider.GOODS_QR, qrcode);
        val.put(QrContentProvider.GOODS_COMMENT, comment);
        val.put(QrContentProvider.GOODS_SCANNED_DATE, idate);
        val.put(QrContentProvider.GOODS_NAME, name);
        val.put(QrContentProvider.GOODS_DESC, desc);
        val.put(QrContentProvider.GOODS_PRICE, (int)(price*100));
        val.put(QrContentProvider.GOODS_QTY, qty);
        val.put(QrContentProvider.GOODS_URL, detailUrl);
        val.put(QrContentProvider.GOODS_SERVER_ID, server_id);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        ContentValues cv = new ContentValues();
        val.put(QrContentProvider.GOODS_IMAGE , out.toByteArray());
        return  val;
    }
    private boolean isSuchRecord(Context ctx){
        StringBuilder builder = new StringBuilder();
        builder.append(QrContentProvider.GOODS_QR).append(" =? ");
        final ContentResolver resolver = ctx.getContentResolver();
        final Cursor cursor = resolver.query(QrContentProvider.CONTENT_URI_GOODS,
                QrContentProvider.GOODS_COLUMNS, builder.toString(),new String[]{qrcode} , null);

        boolean find = cursor.moveToFirst();
        cursor.close();
        return find;
    }

    public static Goods getGoodsByID(int id , Context context){
        if (id == -1) return null;

        Uri uri = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_GOODS , String.valueOf(id));
        Cursor c = context.getContentResolver().query(uri,null ,null , null , null);
        if (!c.moveToFirst()) return null;

        return new Goods(c);

    }

}	
