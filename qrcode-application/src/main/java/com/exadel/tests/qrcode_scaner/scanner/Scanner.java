package com.exadel.tests.qrcode_scaner.scanner;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import com.actionbarsherlock.app.SherlockFragment;
import com.exadel.tests.qrcode_scaner.MainActivity;
import com.exadel.tests.qrcode_scaner.R;
import net.sourceforge.zbar.*;


/**
 *  Use as container for surface view ( camera preview )
 * @author apai
 *
 */
public class Scanner extends SherlockFragment{
	public static final String TAG = "Scanner";
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;

	//TextView scanText;
	Button scanButton;
	public boolean light = false;
	ImageScanner scanner;
    
	public boolean barcodeScanned = false;
	

	static {
		System.loadLibrary("iconv");
	}

	public static Scanner newInstance(){
		return new Scanner();
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    
		super.onActivityCreated(savedInstanceState);
	}

	
	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCamera();
	}

	@Override
	public void onPause() {
		super.onPause();
		mPreview.onPause();
		
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		Log.e(TAG ,"onCreateView");
        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();
       /* Instance barcode scanner                    */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

		View view = inflater.inflate(R.layout.scanner, container, false);

		mPreview = new CameraPreview(getSherlockActivity(), mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout)view.findViewById(R.id.cameraPreview);
        preview.addView(mPreview);
        final OverlayView overlay = new OverlayView(getActivity());
        preview.addView(overlay);
		return view;
	}

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e){
        	e.printStackTrace();
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
        	mPreview.previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();

        }
    }
    
    public boolean getPreviewing(){
    	return mPreview.previewing;
    }
    private Runnable doAutoFocus = new Runnable() {
            public void run() {
                if (mPreview.previewing)
                	try{
                		mCamera.autoFocus(autoFocusCB);
                	}catch(Exception e){
                		e.printStackTrace();
                	}
            }
        };

        
    PreviewCallback previewCb = new PreviewCallback() {
            public void onPreviewFrame(byte[] data, Camera camera) {
                Camera.Parameters parameters = camera.getParameters();
                Size size = parameters.getPreviewSize();

                Image barcode = new Image(size.width, size.height, "Y800");
                barcode.setData(data);

                int result = scanner.scanImage(barcode);
                
                if (result != 0) {
                	mPreview.previewing = false;
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();
                    SymbolSet syms = scanner.getResults();
                    StringBuffer buff = new StringBuffer();
                    for (Symbol sym : syms) buff.append(sym.getData());
                        

                    ((MainActivity)getSherlockActivity()).onQRScanned(buff.toString());
                    barcodeScanned = true;
                    
                }

            }
        };

    /**
     *  Mimic continuous auto-focusing
     */
    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
                autoFocusHandler.postDelayed(doAutoFocus, 1000);
            }
        };

        /***
         * stop camera preview 
         */
	public void startPreview() {
		if (!mPreview.previewing) {
			mPreview.onResume();
			mPreview.previewing = true;
		}
	}
	/**************
	 *  start camera preview 
	 */
	 
	public void stopPreview() {
			mPreview.onPause();
			mPreview.previewing = false;
		
	}
	/**
	 * Turn on or off flash light on camera
	 */
	public void turnOnOffFlash() {
		Parameters params = mCamera.getParameters();
		params.setFlashMode(light?Parameters.FLASH_MODE_OFF:Parameters.FLASH_MODE_TORCH);
		light = !light;
		mCamera.setParameters(params);

	}


}
