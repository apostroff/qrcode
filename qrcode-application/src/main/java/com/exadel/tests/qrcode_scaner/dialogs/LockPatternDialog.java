package com.exadel.tests.qrcode_scaner.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import com.exadel.tests.qrcode_scaner.MainActivity;
import com.exadel.tests.qrcode_scaner.PaidActivity;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.VisaListActivity;
import com.exadel.tests.qrcode_scaner.secure.Crypt;
import group.pals.android.lib.ui.lockpattern.widget.LockPatternUtils;
import group.pals.android.lib.ui.lockpattern.widget.LockPatternView;
import group.pals.android.lib.ui.lockpattern.widget.LockPatternView.Cell;
import group.pals.android.lib.ui.lockpattern.widget.LockPatternView.OnPatternListener;

import java.util.List;

public class LockPatternDialog extends DisFragmentDialog {
	public static final int TYPE_SET_PASSWORD=0;
	public static final int TYPE_EDIT_CARD=1;
	public static final int TYPE_PAY_CARD=2;
	public static final String TYPE = "type";
	public static final int MIN_POINTS_ON_PATTERN = 4;
	public static final int MAX_TRIES = 3;
	
	public static final String TAG = "lockDlg";
	public static final String NEW_PASS = "newPass";
	public static final String CARD_NUMBER = "number";
	public static final String LAST_CARD_NUMBER = "lastnumber";
	public static final String TOTAL_SUM = "totalsum";
	public static final int STATE_FIRST_SET=0;
	public static final int STATE_SECOND_SET=1;
	public static final int STATE_ENTER=2;
	
	private LockPatternView lockView;
	private int try_count = 0;
	private String sha1_pass=null;
	boolean newPass =true;
	Button btNext;
	List<Cell> currentPattern=null;
	String number ;
	
	/***
	 * 	fabric method for create new instance for set pattern password
	 * @return
	 */
	public static LockPatternDialog newInstance(){
		LockPatternDialog dlg = new LockPatternDialog();
		Bundle args = new Bundle();
		args.putBoolean(NEW_PASS, true );
		args.putInt(TYPE, TYPE_SET_PASSWORD);
		dlg.setArguments(args);
		
		return dlg;
	}
	/**
	 * fabric method for enter pattern password
	 * @param number  encoding card number
	 * @param lastnumber last non-encoding number
	 * @param totalsum total goods sum 
	 * @return
	 */
	
	public static LockPatternDialog newInstance(String number , String lastnumber , float totalsum){
		LockPatternDialog dlg = new LockPatternDialog();
		Bundle args = new Bundle();
		args.putBoolean(NEW_PASS, false);
		args.putString(CARD_NUMBER,number);
		args.putString(LAST_CARD_NUMBER,lastnumber);
		args.putFloat(TOTAL_SUM,totalsum);
		args.putInt(TYPE, TYPE_PAY_CARD);
		dlg.setArguments(args);
		return dlg;
	}

	/**
	 * fabric method for enter pattern password for edit card properties
	 * @param number  encoding card number
	 * @return
	 */
	
	public static LockPatternDialog newInstance(String number , String last_numer ){
		Log.e(TAG , "num="+number);
		LockPatternDialog dlg = new LockPatternDialog();
		Bundle args = new Bundle();
		args.putBoolean(NEW_PASS, false);
		args.putString(CARD_NUMBER,number);
		args.putString(LAST_CARD_NUMBER,last_numer);
		args.putInt(TYPE, TYPE_EDIT_CARD);
		dlg.setArguments(args);
		return dlg;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		int style = DialogFragment.STYLE_NORMAL;
		this.setRetainInstance(true);
		setStyle(style,0);
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v  = inflater.inflate(R.layout.lock_pattern_dialog,container , false );
		lockView  = (LockPatternView)v.findViewById(R.id.lockPattern);
		lockView.setOnPatternListener(onPatternLockChange);
		
		btNext = (Button)v.findViewById(R.id.bt_next);
		btNext.setOnClickListener(onNext);
		
		Bundle args = getArguments();
		if (args.containsKey(NEW_PASS)) newPass = args.getBoolean(NEW_PASS);
		if (!newPass) number = args.getString(CARD_NUMBER);
		setWindowParams();

		if (newPass){
			getDialog().setTitle("Please , set password!");
			
			v.findViewById(R.id.tv_lock_dlg_sum).setVisibility(View.GONE);
			v.findViewById(R.id.tv_lock_sum_caption).setVisibility(View.GONE);
			v.findViewById(R.id.bt_lock_dlgreview).setVisibility(View.GONE);
		}else {
			getDialog().setTitle("Please , enter password !");
			((TextView)v.findViewById(R.id.tv_lock_dlg_cap)).setText(R.string.cardnumber);
			((TextView)v.findViewById(R.id.tv_lock_dlg_card)).setText("*******"+args.getString(LAST_CARD_NUMBER));
			setTotalSum(v);
		}
		getDialog().setCanceledOnTouchOutside(false);
		
		final Button btCancel = (Button) v.findViewById(R.id.bt_cancel);
		btCancel.setOnClickListener(onCancel);
		
		return v;
	}
	/***
	 * set parameters to total sum textview and review button
	 * @param v
	 */
	private void setTotalSum(View v) {
		Bundle bundle = getArguments();
		// button for show list of goods
		final Button btReview = (Button)v.findViewById(R.id.bt_lock_dlgreview);		

		if (bundle.containsKey(TOTAL_SUM)){
			((TextView)v.findViewById(R.id.tv_lock_dlg_sum)).setText(String.valueOf(bundle.getFloat(TOTAL_SUM)));
			btReview.setOnClickListener(onReview);
		}else{
			v.findViewById(R.id.tv_lock_dlg_sum).setVisibility(View.GONE);
			v.findViewById(R.id.tv_lock_sum_caption).setVisibility(View.GONE);
			btReview.setVisibility(View.GONE);
		}
	}		

	private OnClickListener onReview = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getActivity() , PaidActivity.class);
			intent.putParcelableArrayListExtra(VisaListActivity.GOODS, getActivity().getIntent().getParcelableArrayListExtra(VisaListActivity.GOODS));
			intent.putExtra(PaidActivity.PAIDED, false); 
			startActivity(intent);
		}
	};
	
	private OnClickListener onCancel= new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
		}
	}; 
	
	private void setWindowParams(){
		Window window= getDialog().getWindow();
		window.setGravity(Gravity.TOP);
		window.setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
	}

	private OnClickListener onNext = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (newPass)
				onSetPassword();
			else 
				onCheckPassword();
		}
	};
	/****
	 * when dialog is used for check password
	 */
	private void onCheckPassword(){
		String sha1_hash= LockPatternUtils.patternToSha1(currentPattern);
		String decode_number  = Crypt.decrypt(number, sha1_hash);
		
		if (decode_number==null||!decode_number.matches("^\\d+$")){
			lockView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
			showToast("Wrong password pattern(");
			try_count++;
			if (try_count>=MAX_TRIES){// if we did maximum tries - go to scann screen
				showMainScren();
			}
		}else{
			int type = getArguments().getInt(TYPE); 
			if (type==TYPE_PAY_CARD)
				(( VisaListActivity)getActivity()).onDecode(sha1_hash);
			if (type==TYPE_EDIT_CARD)
				(( VisaListActivity)getActivity()).onDecodeEdit(sha1_hash);
			dismiss();
		}
			
			
	}
	/**
	 * show Scanner screen with clear top flasg - to block return on Visa and History Screen
	 */
	private void showMainScren() {
		dismiss();
		Intent intent = new Intent(getActivity(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	/***
	 * When dialog used for set password  
	 */
	private void onSetPassword(){
		if (sha1_pass==null){// first password , remember and clear for giving user chance to confirm
			if (currentPattern.size()<MIN_POINTS_ON_PATTERN){
				showToast("Need more then "+MIN_POINTS_ON_PATTERN+" cells!");
			}else{
				sha1_pass = LockPatternUtils.patternToSha1(currentPattern);
				getDialog().setTitle("Confirm password!");
				btNext.setText(R.string.done);
			}
			lockView.clearPattern();
		}else{// user confirm password, compare with first pattern
			String sha_pass2 = LockPatternUtils.patternToSha1(currentPattern);
			if (sha1_pass.equals(sha_pass2)){
				dismiss();

				Fragment dlg = getActivity().getSupportFragmentManager().findFragmentByTag(AddNewCardDialog.TAG);
				if (dlg!=null){
					((AddNewCardDialog)dlg).onSetPassword(sha1_pass);
					
				}else
					showToast("Cant find add card dialog ((");
			}else{
				showToast("Patterns isnt equal!");
				lockView.clearPattern();
			}
		}
	}
	LockPatternView.OnPatternListener onPatternLockChange = new OnPatternListener() {
		
		@Override
		public void onPatternStart() {

		}
		
		@Override
		public void onPatternDetected(List<Cell> pattern) {
			currentPattern = pattern;
		}
		
		@Override
		public void onPatternCleared() {

		}
		
		@Override
		public void onPatternCellAdded(List<Cell> pattern) {
			

		}
	};

	
}
