package com.exadel.tests.qrcode_scaner.adapters;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;

import android.support.v4.widget.CursorAdapter;

import android.text.format.DateFormat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.dialogs.QRCodeDialog;
import com.exadel.tests.qrcode_scaner.domain.Goods;

import java.util.ArrayList;

public class GoodsAdapter extends CursorAdapter {
	public boolean allCheckOnCreate=false;
    private int selected_color;
    /**store check box values in array     */
    final public ArrayList<Boolean> itemChecked = new ArrayList<Boolean>();


    public static class ViewHolder {
        public TextView tvDate;
        public TextView tvName;
        public ImageView imChecked;
        public TextView tvQty;
        public TextView tvSum;
        public View btDetails;
        public ImageView imImage;
    }
	

	public GoodsAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
        selected_color = context.getResources().getColor(R.color.list_view_checked);
	}

	@Override
	public void changeCursor(Cursor cursor) {
		super.changeCursor(cursor);
	}
	
	private void checkItemArray(int count){
		while (itemChecked.size()<count)
			itemChecked.add(allCheckOnCreate);
	}

    /**
     *  on set data to already created container

     */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		checkItemArray(cursor.getCount());
		ViewHolder holder = (ViewHolder) view.getTag();
		Goods record = new Goods(cursor); 
		holder.tvName.setText(record.name);


		holder.tvQty.setText("( "+String.valueOf(record.qty)+" )");
        holder.tvSum.setText(String.valueOf(record.getSum()));
	    holder.tvDate.setText(DateFormat.format(QRCodeDialog.DATE_FORMAT,record.idate).toString()  );
        holder.imImage.setImageBitmap(record.imageBitmap);


        final int pos = cursor.getPosition();
        view.setBackgroundColor(itemChecked.get(pos)?selected_color:Color.TRANSPARENT);
        holder.imChecked.setImageResource(itemChecked.get(pos)?R.drawable.items_check_pressed:R.drawable.items_check);
	}


    /***
     *  on create new view container
     */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup container) {
		LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.goods_row_layout,null,true);

        ViewHolder holder = new ViewHolder();
        
        holder.tvName = (TextView) rowView.findViewById(R.id.tv_goodrow_name);

        holder.tvQty = (TextView) rowView.findViewById(R.id.tv_goodrow_qty);
        holder.tvSum = (TextView) rowView.findViewById(R.id.tv_goodrow_sum);
        holder.tvDate= (TextView) rowView.findViewById(R.id.tv_goodrow_date);
        holder.btDetails = rowView.findViewById(R.id.bt_goodrow_details);
        holder.imImage = (ImageView)rowView.findViewById(R.id.im_goodrow_image);
        holder.imChecked = (ImageView)rowView.findViewById(R.id.im_goodrow_checked);
        rowView.setTag(holder);
		return rowView;
	}



}
