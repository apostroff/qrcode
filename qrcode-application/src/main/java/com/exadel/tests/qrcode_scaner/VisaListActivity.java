package com.exadel.tests.qrcode_scaner;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.exadel.tests.qrcode_scaner.adapters.VisaAdapter;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;
import com.exadel.tests.qrcode_scaner.dialogs.*;
import com.exadel.tests.qrcode_scaner.domain.Goods;
import com.exadel.tests.qrcode_scaner.domain.PayCard;
import com.exadel.tests.qrcode_scaner.secure.Crypt;
import com.exadel.tests.qrcode_scaner.secure.MySSLSocketFactory;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.message.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/***
 *  List of stored payment cards
 * @author apai
 *
 */
public class VisaListActivity extends AbstractCustomABSActivity
							implements LoaderCallbacks<Cursor>  , OnItemClickListener, OnMenuItemClickListener{

    public static final String URL_ADD_PAYMENT="https://88.198.170.125/goods/ordercreate";

	public static final String GOODS="goods";
	
	// menu
	public static final int MENU_ADD=0;
	public static final int MENU_EDIT=1;
	public static final int MENU_DELETE=2;
	public static final int MENU_BUY=3;
	public static final int MENU_CANCDEL=4;
    ListView mListView     ;
	VisaAdapter mAdapter; 
	ArrayList<Goods> ent_data;
	//private TextView tvHint;
	private int mRowClicked=-1;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.visas_layout);
        mListView  = (ListView)findViewById(R.id.lv_visas);
		mAdapter = new VisaAdapter(this, null, false); 
		//tvHint = (TextView)findViewById(R.id.tv_visas_hint);
		mListView.setAdapter(mAdapter);
		getSupportLoaderManager().initLoader(0, null, this);
		
		ent_data= getIntent().getParcelableArrayListExtra(GOODS);
		// if list of goods is present in the intent - create checkout fragment for display it
		if (ent_data!=null) {
			FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
			trans.add(R.id.checkoutfragment , CheckOut.newInstance(ent_data));
			trans.commit();
		}
		//	tvHint.setVisibility(View.GONE);

		mListView.setOnItemClickListener(this);
		mListView.setClickable(true);
		mListView.setSelected(true);
		mListView.setEnabled(true);


	}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(Menu.NONE , MENU_ADD, Menu.NONE ,R.string.add)
    		.setIcon(R.drawable.ic_plus)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );

    	menu.add(Menu.NONE , MENU_EDIT, Menu.NONE ,R.string.edit)
			.setIcon(R.drawable.ic_pensil)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );
    	
    	menu.add(Menu.NONE , MENU_DELETE, Menu.NONE ,R.string.delete)
    		.setIcon(R.drawable.ic_cross)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );


    	menu.add(Menu.NONE , MENU_BUY, Menu.NONE ,R.string.pay)
			.setOnMenuItemClickListener(this)
			.setEnabled(isAnyStoredGoods()||ent_data!=null)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |  MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			
    	menu.add(Menu.NONE , MENU_CANCDEL, Menu.NONE ,R.string.cancel)
    		.setIcon(R.drawable.ic_undo)
			.setOnMenuItemClickListener(this)
			.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM );
    	return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
	    // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
        Uri baseUri = QrContentProvider.CONTENT_URI_VISA;
        return new CursorLoader(VisaListActivity.this, baseUri,QrContentProvider.VISA_COLUMNS, null, null,null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);	
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
	}
	
	 //{VISA_ID,VISA_NUMBER,VISA_YEAR,VISA_MONTH,VISA_OWNER,VISA_CW2, VISA_TYPE, VISA_LAST_NUMBER} 

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long viewid) {
        Log.e("VISA" , "ALJHGLUGLJGHLJGH");
		mAdapter.setSelected(pos, view);
		/*if(mAdapter.getSelected()==-1)
			tvHint.setText(R.string.unselected_visa_hint);
		else
			tvHint.setText(R.string.selected_visa_hint);
			*/
	}
	
	public void onDecode(String decoding){
		if (mRowClicked==-1) return ;
		Cursor c= mAdapter.getCursor();
		c.moveToPosition(mRowClicked);
		
		
		//int type = c.getInt(6);
		String number = Crypt.decrypt(c.getString(1), decoding) ;
		int year = c.getInt(2);
		int month = c.getInt(3);
		String cw2  =  Crypt.decrypt(c.getString(5) , decoding ) ;
		String owner = c.getString(4);
		//int limit = c.getInt(8);
		String last = c.getString(7);
		buy( number, month, year, cw2, owner ,  last);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode,Intent data){
		Log.e("Activity" , ""+resultCode);
	
		switch (resultCode){
			case Activity.RESULT_OK:
				buy("",0,0,"","", PayCard.PAYPAL);
				break;
		}
		super.onActivityResult(requestCode, resultCode,data);
	}

	/***
	 * BUUYYY
	 *
	 */
	//TODO we have all data for buying something
	public void buy( String number ,  int month , int year , String cw2 , String owner   , String last_4){
		if (ent_data==null||ent_data.size()==0) return ;
		//**** send data to server
        WaitProgresDialog dlg = WaitProgresDialog.newInstance(R.string.sendwait);
        dlg.show(getSupportFragmentManager() ,WaitProgresDialog.TAG );

        Toast msgs  = Toast.makeText(VisaListActivity.this,"URL="+URL_ADD_PAYMENT , Toast.LENGTH_LONG);
        msgs.setGravity(Gravity.CENTER , 0,0);
        msgs.show();

        new SendAddPaymentRequest().execute(number,owner ,last_4 );

		sendDataToServer(number,month , year , cw2, owner,getTotalSum());
	}

    private void onPaided(String last_4){
        // delete paid goods from stored ents
        // TODO uncommit this after test
        //delete_ents();
        WaitProgresDialog dlg = (WaitProgresDialog)getSupportFragmentManager().findFragmentByTag(WaitProgresDialog.TAG);
        dlg.dismiss();
        savePaid(last_4);
        showPaidedActivity();
    }

	private void savePaid(String last_4) {
		long curtime =System.currentTimeMillis();
		ContentResolver resolv = getContentResolver();
		
		for (Goods row : ent_data){
			ContentValues values  = new ContentValues();
			values.put(QrContentProvider.PAID_NAME, row.name);
			values.put(QrContentProvider.PAID_DESC, row.desc);
			values.put(QrContentProvider.PAID_PRICE, (int)(row.getPrice()*100));
            values.put(QrContentProvider.PAID_QTY, row.qty);
			values.put(QrContentProvider.PAID_URL, row.detailUrl);
			values.put(QrContentProvider.PAID_DATE, curtime);
			values.put(QrContentProvider.PAID_CARD, last_4);
			
			try{
				resolv.insert(QrContentProvider.CONTENT_URI_PAID, values);
		
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
	}

	/***
	 * show thank you screen with list of goods
	 */

	private void showPaidedActivity() {
		Intent intent = new Intent(this , PaidActivity.class);
		intent.putParcelableArrayListExtra(GOODS, ent_data);
		intent.putExtra(PaidActivity.PAIDED, true);
		Random r = new Random();
		intent.putExtra(PaidActivity.TRANS_ID ,  String.valueOf(r.nextInt(Integer.MAX_VALUE)));
		startActivity(intent);
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch(item.getItemId()){
			case MENU_ADD : 
				AddNewCardDialog dlg = AddNewCardDialog.newInstance();
				dlg.show(getSupportFragmentManager(), AddNewCardDialog.TAG);
				break;
			case MENU_EDIT :
				editCard();
				break;
			case MENU_BUY :
				if (ent_data==null) ent_data = getAllStoredGoods();
				if (ent_data!=null) pay();
				//else onNoGoodPayPressed();  
				break;
			case MENU_DELETE : 
				deleteCard();
				break;
			case MENU_CANCDEL : 
				finish();
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
				break;
		}
		return false;
	}
	private ArrayList<Goods> getAllStoredGoods() {
		Uri goods_uri = QrContentProvider.CONTENT_URI_GOODS;
		Cursor c = getContentResolver().query(goods_uri, QrContentProvider.GOODS_COLUMNS, null, null, null);
		ArrayList<Goods> res = new ArrayList<Goods>();
		
		if (!c.moveToFirst()) {
			c.close();
			return res;
		}
		do{
			res.add( new Goods(c));
		}while (c.moveToNext());
		c.close();
		return res;
	}

	private void editCard() {
		if (mAdapter.getSelected()>-1){
			mAdapter.getCursor().moveToPosition(mAdapter.getSelected());
			String encodingnumber = mAdapter.getCursor().getString(1);
			String last4 = mAdapter.getCursor().getString(7);
			
			LockPatternDialog dlg  = LockPatternDialog.newInstance(encodingnumber , last4);
			dlg.show(getSupportFragmentManager(), LockPatternDialog.TAG);
		}
	}
	/**
	 * call back after decoding to edit visa properties
	 * @param decoding  key for decode
	 */
	public void onDecodeEdit(String decoding){
		if (mAdapter.getSelected()>-1){
			final Cursor c   = mAdapter.getCursor(); 
		
			c.moveToPosition(mAdapter.getSelected());
			int id = c.getInt(0);
			int type = c.getInt(6);    
			String number = Crypt.decrypt(c.getString(1) , decoding ) ;
			int year = c.getInt(2);
			int month = c.getInt(3);
			String cw2  =  Crypt.decrypt(c.getString(5) , decoding ) ;
			String owner = c.getString(4);
			int limit = c.getInt(8);
			AddNewCardDialog dlg = AddNewCardDialog.newInstance(id, type, number, month, year, cw2, owner, limit); 
			dlg.show(getSupportFragmentManager(), AddNewCardDialog.TAG);
		}
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	/***
	 *  payment by selected card 
	 */
	private void pay() {
		if (mAdapter.getSelected()==-1){
			Toast.makeText(this, "Please  , select card!", Toast.LENGTH_LONG).show();
			return ;
		}
		
		
		
		Cursor cursor = mAdapter.getCursor();
		cursor.moveToPosition(mAdapter.getSelected());
		int limit = cursor.getInt(8);
		if (getTotalSum()>limit){
			PromtDialog dlg = PromtDialog.newInstance(getResources().getString(R.string.over_limit));
			dlg.show(getSupportFragmentManager(), PromtDialog.TAG);
			return ;
		}
		String number = cursor.getString(1);// encoding cardnumber
		String lnumber = cursor.getString(7); // last 4 number digit
		LockPatternDialog dlg = LockPatternDialog.newInstance(number , lnumber , getTotalSum());
		dlg.show(getSupportFragmentManager(), LockPatternDialog.TAG);
		mRowClicked =mAdapter.getSelected(); 
		mAdapter.resetSelection();
	}
	/****
	 * on delete payment card record 
	 */

	private void deleteCard() {
		int del_count=0;
		if (mAdapter.getSelected()>-1){
            final Cursor cursor =    mAdapter.getCursor();
            cursor.moveToPosition(mAdapter.getSelected());
			Uri delUri = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_VISA, mAdapter.getCursor().getString(0));
			int count  = getContentResolver().delete(delUri, null, null);
			del_count+=count;
			Toast.makeText(VisaListActivity.this, "Deleted "+del_count+" rows", Toast.LENGTH_LONG).show();
            // mark next view
            if (cursor.getCount()==0) {
                mAdapter.resetSelection();
            }else   {
                int newSelected = Math.min(cursor.getCount()-2,mAdapter.getSelected());  // -2 = one deleted and one  zero
                mAdapter.setSelected(newSelected,getViewByPos(newSelected));

            }
            mAdapter.notifyDataSetChanged();
		}

	}
	/**
	 * if url adrr set in shared preferences - we send post rest query to server
	 */
	public void sendDataToServer(String number, int month, int year, String cw2, String owner, float totalsum){
		SharedPreferences prefs = getSharedPreferences(MainActivity.SHOW_AGAIN, Activity.MODE_PRIVATE);
		String url = prefs.getString(PreferenceDialog.PREF_URL, "");
		
		if (url.length()==0) return ;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("number", number));
        nameValuePairs.add(new BasicNameValuePair("month",String.valueOf(month)));
        nameValuePairs.add(new BasicNameValuePair("year",String.valueOf(year)));
        nameValuePairs.add(new BasicNameValuePair("cw2", cw2));
        nameValuePairs.add(new BasicNameValuePair("owner", owner));
        nameValuePairs.add(new BasicNameValuePair("sum",String.valueOf(totalsum)));
        new SendToServer(url , nameValuePairs).execute();
	}

    /***
     * send post request to server
     * card number is first element in array
     * buyer second
     * last4 third
     */
    public class SendAddPaymentRequest extends AsyncTask<String , Void , Boolean>{

        String last4;
        @Override
        protected Boolean doInBackground(String... prms) {
            last4 = prms[2];
            return sendPaymentRequestToServer(prms[0],prms[1]); // cardnumber , buyer
        }
        @Override
        protected void onPostExecute(Boolean result) {
            if (result){
                onPaided(last4);
                Toast.makeText(VisaListActivity.this, "Message sended successfully", Toast.LENGTH_SHORT).show();
            }else   {

                WaitProgresDialog dlgw = (WaitProgresDialog)getSupportFragmentManager().findFragmentByTag(WaitProgresDialog.TAG);
                dlgw.dismiss();

                PromtDialog dlg = PromtDialog.newInstance("Sending message failed");
                dlg.show(getSupportFragmentManager() , PromtDialog.TAG);
                Toast.makeText(VisaListActivity.this, "Sending message failed", Toast.LENGTH_SHORT).show();
            }

        }
    }
	/***
	 * create new thread to send post request
	 * @author apai
	 *
	 */
	public class SendToServer extends AsyncTask<Void, Void, Boolean>{
		private String url;
		List<NameValuePair> nameValuePairs;
		public SendToServer(String url ,List<NameValuePair> param){
			this.url = url;
			this.nameValuePairs=param ;


		}
		@Override
		protected Boolean doInBackground(Void... params) {

			HttpClient httpclient = new DefaultHttpClient();
			if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
			
			HttpPost httppost = new HttpPost( url); // server IP and URL
			try {
		         // Add your data
		         httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		         // Execute HTTP Post Request
		         //
		         httpclient.execute(httppost);
			} catch (ClientProtocolException e) {
		         return false;
			} catch (IOException e) {
		         return false;
			}
			return true;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			if (result)
				Toast.makeText(VisaListActivity.this, "Message sended successfully", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(VisaListActivity.this, "Sending message failed", Toast.LENGTH_SHORT).show();
		}
	}

    private String getJson(String number,  String owner)  {
        JSONObject paymentJson= new JSONObject();

        try {
            paymentJson.put("date", DateFormat.format("yyyy-MM-dd",System.currentTimeMillis() ).toString())  ;
            paymentJson.put("paymentcard",number);
            paymentJson.put("buyer",owner);
            paymentJson.put("rows",getJsonGoods());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSON PAYMENT",paymentJson.toString());
        return paymentJson.toString();
    }
    private JSONArray getJsonGoods(){
        JSONArray res = new JSONArray();
        for (Goods good:ent_data)
            res.put(good.getJSONObjectForPost());

        return  res;
    }

    private boolean sendPaymentRequestToServer(String number,  String owner){

        HttpClient httpClient = MySSLSocketFactory.getTrustAllHttpsClient();

        HttpPost httpPost = new HttpPost(URL_ADD_PAYMENT);
        Log.e("SEND JSON" , getJson(number,  owner));
        try {
            httpPost.setEntity(new StringEntity(getJson(number,  owner), "UTF8"));
        } catch (UnsupportedEncodingException e) {
            // never reach here
            e.printStackTrace();
            return false;
        }
        httpPost.setHeader("Content-type", "application/json");
        try {
            HttpResponse resp = httpClient.execute(httpPost);
            Log.e("HttpResponse" , ""+resp.getStatusLine().getStatusCode());
            return  (resp.getStatusLine().getStatusCode() == 200);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }


    }

	/**
	 * get sum of prices in array : ent_data
	 * @return  total sum
	 */
			
	private float getTotalSum(){
		float t=0f;
		for (Goods row: ent_data) t+=row.getSum();
		return t;
	}
	
	/**
	 *  user press button pay - without goods
	 */

    private void onNoGoodPayPressed(){
		
		Intent intent  = new Intent(this , HistoryActivity.class);
		intent.putExtra(HistoryActivity.ALL_CHECK, true);
		startActivity(intent);
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	/**
	 * 
	 */
	private boolean isAnyStoredGoods(){
		Uri goods  = QrContentProvider.CONTENT_URI_GOODS;
		Cursor c = getContentResolver().query(goods, QrContentProvider.GOODS_COLUMNS, null, null, null);
		boolean res  = c.moveToFirst();
		c.close();
		return res;
	}
	/***
	 * delete goods from history
	 */

    private void delete_ents(){
		for(Goods  row: ent_data){
			Uri uri = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_GOODS,String.valueOf(row.id));
			try{
				getContentResolver().delete(uri, null, null);
			}catch(IllegalArgumentException e){
				Log.e("delete" , e.toString());
			}
		}
	}


    private View getViewByPos(int wantedPosition ) {

        int firstPosition = mListView.getFirstVisiblePosition() - mListView.getHeaderViewsCount(); // This is the same as child #0
        int wantedChild = wantedPosition - firstPosition;
    // Say, first visible position is 8, you want position 10, wantedChild will now be 2
    // So that means your view is child #2 in the ViewGroup:
        if (wantedChild < 0 || wantedChild >= mListView.getChildCount()) {
            return null;
        }
        // Could also check if wantedPosition is between listView.getFirstVisiblePosition() and listView.getLastVisiblePosition() instead.
        return mListView.getChildAt(wantedChild);
    }

}
