package com.exadel.tests.qrcode_scaner.dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.actionbarsherlock.app.SherlockDialogFragment;

/**
 * Created with IntelliJ IDEA.
 * User: apai
 * Date: 20.02.13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
public class WaitProgresDialog extends SherlockDialogFragment {

    public static final String TAG = "PROGRESS";
    private static final String WAIT_MSG = "msg";

    /***
     * fabric method for create new instance
     * @param msgid
     * @return new instance of dialog
     */
    public static WaitProgresDialog newInstance(int msgid){
        WaitProgresDialog dlg = new WaitProgresDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(WAIT_MSG,   msgid);
        dlg.setArguments(bundle);
        return dlg;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        int style = DialogFragment.STYLE_NORMAL;

        this.setRetainInstance(true);
        setStyle(style,0);
        super.onCreate(savedInstanceState);
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dlg = new ProgressDialog(getActivity());
        dlg.setCancelable(false);
        dlg.setCanceledOnTouchOutside(false);
        String msg= getResources().getString(getArguments().getInt(WAIT_MSG));
        dlg.setMessage(msg);
        return dlg;
    }
    DialogInterface.OnClickListener onClose = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dismiss();

        }


    };
}
