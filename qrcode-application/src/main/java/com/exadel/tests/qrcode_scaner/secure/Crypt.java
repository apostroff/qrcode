package com.exadel.tests.qrcode_scaner.secure;


import android.os.Build;
import android.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;
import java.util.Arrays;
/***
 *  Use for AES crypt card data
 * @author apai
 *
 */



public class Crypt {
	/**
	 * we need 32 length string for key
	 * @param key
	 * @return
	 */
	public static String getValidKey(String key){
		if (key.length()==32) return key;
		String symKeyHex;
		if (key.length()>32)
			symKeyHex = key.substring(0,32);
		else {
			char[] solt = new char[32-key.length()];
			Arrays.fill(solt, '0');
			StringBuilder b = new StringBuilder(key);
			b.append(solt);
			symKeyHex = b.toString();
		}
		return symKeyHex;
	}
	public static String encrypt(final String plainMessage,final String key) {
		String symKeyHex = getValidKey(key);
	    final byte[] symKeyData = Base64.decode(symKeyHex, Base64.DEFAULT); 

	    final byte[] encodedMessage; 
	    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.FROYO)
			try {
				encodedMessage = plainMessage.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e1) {
				//  never rich this
				e1.printStackTrace();
				return null;
			}
		else 
	    	encodedMessage = plainMessage.getBytes(Charset.forName("UTF-8"));
	    
	    try {
	    	
	        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        final int blockSize = cipher.getBlockSize();

	        // create the key
	        final SecretKeySpec symKey = new SecretKeySpec(symKeyData, "AES");

	        // generate random IV using block size (possibly create a method for
	        // this)
	        final byte[] ivData = new byte[blockSize];
	        final SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
	        rnd.nextBytes(ivData);
	        final IvParameterSpec iv = new IvParameterSpec(ivData);

	        cipher.init(Cipher.ENCRYPT_MODE, symKey, iv);

	        final byte[] encryptedMessage = cipher.doFinal(encodedMessage);

	        // concatenate IV and encrypted message
	        final byte[] ivAndEncryptedMessage = new byte[ivData.length
	                + encryptedMessage.length];
	        System.arraycopy(ivData, 0, ivAndEncryptedMessage, 0, blockSize);
	        System.arraycopy(encryptedMessage, 0, ivAndEncryptedMessage,blockSize, encryptedMessage.length);
	        		//DatatypeConverter.printBase64Binary(ivAndEncryptedMessage);
	        final String ivAndEncryptedMessageBase64 = Base64.encodeToString(ivAndEncryptedMessage, Base64.DEFAULT); 

	        return ivAndEncryptedMessageBase64;
	    } catch (InvalidKeyException e) {
	        throw new IllegalArgumentException(
	                "key argument does not contain a valid AES key");
	    } catch (GeneralSecurityException e) {
	        throw new IllegalStateException(
	                "Unexpected exception during encryption", e);
	    }
	}

	public static String decrypt(final String ivAndEncryptedMessageBase64,final String key) {
		String symKeyHex = getValidKey(key);
	    final byte[] symKeyData = Base64.decode(symKeyHex, Base64.DEFAULT); 

	    final byte[] ivAndEncryptedMessage = Base64.decode(ivAndEncryptedMessageBase64, Base64.DEFAULT); 
	    try {
	        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	        final int blockSize = cipher.getBlockSize();

	        // create the key
	        final SecretKeySpec symKey = new SecretKeySpec(symKeyData, "AES");

	        // retrieve random IV from start of the received message
	        final byte[] ivData = new byte[blockSize];
	        System.arraycopy(ivAndEncryptedMessage, 0, ivData, 0, blockSize);
	        final IvParameterSpec iv = new IvParameterSpec(ivData);

	        // retrieve the encrypted message itself
	        final byte[] encryptedMessage = new byte[ivAndEncryptedMessage.length
	                - blockSize];
	        System.arraycopy(ivAndEncryptedMessage, blockSize,
	                encryptedMessage, 0, encryptedMessage.length);

	        cipher.init(Cipher.DECRYPT_MODE, symKey, iv);

	        final byte[] encodedMessage = cipher.doFinal(encryptedMessage);

	        // concatenate IV and encrypted message
	        
	        String message;
	        if (Build.VERSION.SDK_INT==Build.VERSION_CODES.FROYO){
	        	try {
					message	= new String(encodedMessage,"UTF-8");
				} catch (UnsupportedEncodingException e) {
					//  never rich this
					e.printStackTrace();
					return null;
				}
	        }else 
	        	message	= new String(encodedMessage,Charset.forName("UTF-8"));

	        return message;
	    } catch (InvalidKeyException e) {
	        throw new IllegalArgumentException("key argument does not contain a valid AES key");
	    } catch (BadPaddingException e) {
	        // you'd better know about padding oracle attacks
	        return null;
	    } catch (GeneralSecurityException e) {
	        throw new IllegalStateException(
	                "Unexpected exception during decryption", e);
	    }
	}
}
