package com.exadel.tests.qrcode_scaner.adapters;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.exadel.tests.qrcode_scaner.MainActivity;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.database.QrContentProvider;

public class VisaAdapter extends CursorAdapter  {
    public static final String VISA_DEFAULT_PREF = "visa_def";
	private static final int[] visa_types = {R.drawable.visa,R.drawable.maestro,R.drawable.mastercard};
	private static final String MASK = "********";
	private int selected=-1;
	private View selectedView=null;
	private int selected_color;
    private int mDefaultCardID=-1;

    static class ViewHolder {
        public TextView tvLastNumber;
        public TextView tvDate;
        public ImageView imType;
        public ImageView imCheck;
        public TextView tvLimit;
    }
	
	
	public VisaAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		selected_color = context.getResources().getColor(R.color.visarow_checked);
        SharedPreferences pref = context.getSharedPreferences(MainActivity.SHOW_AGAIN, Activity.MODE_PRIVATE);
        final  int defCardIdfrompref = pref.getInt(VISA_DEFAULT_PREF,-1);
        if (defCardIdfrompref==-1) return;
        if (cardExistsByID(context,defCardIdfrompref))  mDefaultCardID=    defCardIdfrompref;


	}

    public static boolean cardExistsByID(Context context , int id){

        Uri uri = Uri.withAppendedPath(QrContentProvider.CONTENT_URI_VISA,String.valueOf(id));
        Cursor c  =  context.getContentResolver().query(uri , null , null , null  , null);
        Log.e("visadapter", "id="+id+"  "+(c.moveToFirst()?"yes":"no"));
        return  c.moveToFirst();

    }
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();
		
		//VISA_ID,VISA_NUMBER,VISA_YEAR,VISA_MONTH,VISA_OWNER,VISA_CW2, VISA_TYPE, VISA_LAST_NUMBER , VISA_LIMIT} ;		
		holder.tvLastNumber.setText(MASK+cursor.getString(7));
		StringBuilder build = new StringBuilder();
		build.append(cursor.getString(3)).append("/").append(cursor.getString(2));
		holder.tvDate.setText(build.toString());
		int limit = cursor.getInt(8);
		if (limit==0)
			holder.tvLimit.setText(R.string.unlimit);
		else 
			holder.tvLimit.setText(String.valueOf(limit));
			
		try{
			int visa_type = cursor.getInt(6);
			holder.imType.setImageResource(visa_types[visa_type]);
		}catch(ArrayIndexOutOfBoundsException e){// 
			holder.imType.setImageResource(R.drawable.visa);
		}
		if (cursor.getPosition()==selected) {
			view.setBackgroundColor(selected_color);
            holder.imCheck.setImageResource(R.drawable.ic_checked);
        }else{
			view.setBackgroundColor(Color.TRANSPARENT);
            holder.imCheck.setImageDrawable(null);
        }
	}
	

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup container) {
		LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.visa_row_new,null,true);

        ViewHolder holder = new ViewHolder();
        holder.imType = (ImageView)rowView.findViewById(R.id.im_visa_type);
        holder.tvLastNumber = (TextView)rowView.findViewById(R.id.tv_visa_row_cardnumber);
        holder.tvDate = (TextView)rowView.findViewById(R.id.tv_visa_row_date);
        holder.tvLimit = (TextView)rowView.findViewById(R.id.tv_visa_row_cardlimit);
        holder.imCheck = (ImageView)rowView.findViewById(R.id.im_visa_checked);

        rowView.setTag(holder);
        Log.e("newView" , "mDefaultCardID="+mDefaultCardID+" cursor.getInt(0)="+cursor.getInt(0))  ;
        if (cursor.getInt(0)==mDefaultCardID){
            selected = cursor.getPosition();
            selectedView=rowView;
        }
        if (mDefaultCardID==-1&&selected==-1&&cursor.getPosition()==0){
            selected=0;
            selectedView=rowView;
        }
		return rowView;
	}
	public void setSelected(int pos , View v){
        if (selected==pos) return;
		unselect();

        selected=pos;
        selectedView = v;
        if (v!=null){
            selectedView.setBackgroundColor(selected_color);
            ViewHolder holder = (ViewHolder) v.getTag();
            holder.imCheck.setImageResource(R.drawable.ic_checked);
        }
	}
	public int getSelected(){
		return selected;
	}

	private void unselect() {
		if (selectedView!=null)    {
			selectedView.setBackgroundColor(Color.TRANSPARENT);
            ViewHolder holder = (ViewHolder) selectedView.getTag();
            holder.imCheck.setImageDrawable(null);

        }
	}
	public void resetSelection(){
		unselect();
		selected=-1;
	}



}
