package com.exadel.tests.qrcode_scaner;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.exadel.tests.qrcode_scaner.domain.Goods;
import com.paypal.android.MEP.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class PayPalFragment extends Fragment implements OnClickListener{
    public static final String TAG  = "PayPalFragment";
	public static final String  RECIPIENT = "hello.from@donetsk.by";
	public static final String  MERCHANT = "Some unkown vendor";
	public static final String  MEMO = "Hi! I'm making a memo for a simple payment.";
	public static final BigDecimal TAX = new BigDecimal("0.2");
	 
	public static class PayPalResult implements PayPalResultDelegate , Serializable{
		private static final long serialVersionUID = 10001L;

		@Override
		public void onPaymentCanceled(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onPaymentFailed(String arg0, String arg1, String arg2,String arg3, String arg4) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onPaymentSucceeded(String arg0, String arg1) {
			// TODO Auto-generated method stub
		}
	}
	
	
	// The PayPal server to be used - can also be ENV_NONE and ENV_LIVE
	private static final int PAYPAL_SERVER_TYPE = PayPal.ENV_SANDBOX;
	// The ID of your application that you received from PayPal
	private static final String APP_ID = "APP-80W284485P519543T";


	// This is passed in for the startActivityForResult() android function, the value used is up to you
	private static final int request = 1;
		
	protected static final int INITIALIZE_SUCCESS = 0;
	protected static final int INITIALIZE_FAILURE = 1;
	private RelativeLayout relLayout;
	 
	TextView tvInit ;
	private CheckoutButton launchPayPalButton;
	
		@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		relLayout = new RelativeLayout(getActivity());
		
		final ArrayList<Goods> args = getActivity().getIntent().getParcelableArrayListExtra(VisaListActivity.GOODS);
		// if its no any goods data in activity intent return empty layout
		if (args==null) return relLayout;
		
		relLayout = new RelativeLayout(getActivity());
		relLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

		relLayout.setPadding(10, 10, 10, 10);
		tvInit = new TextView(getActivity());
		tvInit.setText("Paypal initialization...");
		relLayout.addView(tvInit);
		
		new InitPayPal().execute(); // init paypal library
		
		return relLayout;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		if (launchPayPalButton!=null) launchPayPalButton.updateButton();
		super.onResume();
	}
	
	
	/***
	 * thread for init paypal library
	 * @author apai
	 *
	 */
	private class InitPayPal extends AsyncTask<Void, Void, Integer>{
		@Override
		protected void onPostExecute(Integer result) {
			switch(result){
	    	case INITIALIZE_SUCCESS:
	    		setupPayPalButton();
	            break;
	    	case INITIALIZE_FAILURE:
	    		showPayPalInitFailure();
	    		break;
			}
		}
		@Override
		protected Integer doInBackground(Void... params) {
			PayPal pp = PayPal.getInstance();
			// If the library is already initialized, then we don't need to initialize it again.
			if(pp == null) {
				// This is the main initialization call that takes in your Context, the Application ID, and the server you would like to connect to.
				pp = PayPal.initWithAppID(getActivity(), APP_ID, PAYPAL_SERVER_TYPE);
				// -- These are required settings.
	        	pp.setLanguage("en_US"); // Sets the language for the library.
	        	// -- These are a few of the optional settings.
	        	// Sets the fees payer. If there are fees for the transaction, this person will pay for them. Possible values are FEEPAYER_SENDER,
	        	// FEEPAYER_PRIMARYRECEIVER, FEEPAYER_EACHRECEIVER, and FEEPAYER_SECONDARYONLY.
	        	pp.setFeesPayer(PayPal.FEEPAYER_EACHRECEIVER); 
	        	// Set to true if the transaction will require shipping.
	        	pp.setShippingEnabled(false);
	        	// Dynamic Amount Calculation allows you to set tax and shipping amounts based on the user's shipping address. Shipping must be
	        	// enabled for Dynamic Amount Calculation. This also requires you to create a class that implements PaymentAdjuster and Serializable.
	        	pp.setDynamicAmountCalculationEnabled(false);
	        	// --
            }
			// The library is initialized so let's create our CheckoutButton and update the UI.
			if (PayPal.getInstance().isLibraryInitialized()) 
				return INITIALIZE_SUCCESS;
			else 
				return INITIALIZE_FAILURE;

		}
	}
	
	public void setupPayPalButton(){
        if (getActivity()==null) return;
		tvInit.setText("");
		tvInit.setVisibility(View.GONE);
		PayPal paypalObj = PayPal.getInstance();
		
		launchPayPalButton = paypalObj.getCheckoutButton(getActivity(), PayPal.BUTTON_194x37, CheckoutButton.TEXT_PAY);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		//params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		launchPayPalButton.setLayoutParams(params);
		launchPayPalButton.setOnClickListener(this);
		relLayout.addView(launchPayPalButton);
	}
	public void showPayPalInitFailure(){
        if (getActivity()!=null){
            Toast.makeText(getActivity(), "Cant init paypal library", Toast.LENGTH_LONG).show();
            tvInit.setText("Fail to init PayPal");
        }
	}

	@Override
	public void onClick(View v) {
		PayPalPayment payment = getSimplePayment();
        if (payment==null){
            Log.e("Paypal" , "paypal error");
            return;
        }
		// Use checkout to create our Intent.
		Intent checkoutIntent = PayPal.getInstance().checkout(payment, getActivity(),new PayPalResult() );
		// Use the android's startActivityForResult() and pass in our Intent. This will start the library.
    	startActivityForResult(checkoutIntent, request);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode,Intent data){
		if (requestCode!=request) {
			super.onActivityResult(requestCode, resultCode, data);
			return ;
		}
		switch(resultCode) {
			case Activity.RESULT_OK:
				//The payment succeeded
				String payKey = data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
				//Tell the user their payment succeeded
				Toast.makeText(getActivity(), "Payment successfull : key "+payKey, Toast.LENGTH_LONG).show();
				break;
			case Activity.RESULT_CANCELED:
				//The payment was canceled
				//Tell the user their payment was canceled
				Toast.makeText(getActivity(), "Payment canceled", Toast.LENGTH_LONG).show();
				break;
			case PayPalActivity.RESULT_FAILURE:
				//The payment failed -- we get the error from the EXTRA_ERROR_ID and EXTRA_ERROR_MESSAGE
				String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                Log.e("errorID" , "errorID="+errorID+" data "+data.getExtras().toString());
				String errorMessage = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
				//Tell the user their payment was failed.
				Toast.makeText(getActivity(), "Payment failed :"+errorMessage, Toast.LENGTH_LONG).show();

		}
	}
	
	
	/**
	 * Create a PayPalPayment which is used for simple payments.
	 * 
	 * @return Returns a PayPalPayment. 
	 */
	private PayPalPayment getSimplePayment() {
		
		ArrayList<Goods> goods = getActivity().getIntent().getParcelableArrayListExtra(VisaListActivity.GOODS);
    	if (goods==null) {
            Log.e("PayPal" , "NO GOOD FOUND");
            return null;
        }
    	
		// Create a basic PayPalPayment.
		PayPalPayment payment = new PayPalPayment();
		// Sets the currency type for this payment.
    	payment.setCurrencyType("USD");
    	// Sets the recipient for the payment. This can also be a phone number.
    	payment.setRecipient(RECIPIENT);

    	// Sets the payment type. This can be PAYMENT_TYPE_GOODS, PAYMENT_TYPE_SERVICE, PAYMENT_TYPE_PERSONAL, or PAYMENT_TYPE_NONE.
    	payment.setPaymentType(PayPal.PAYMENT_TYPE_GOODS);
    	
    	// PayPalInvoiceData can contain tax and shipping amounts. It also contains an ArrayList of PayPalInvoiceItem which can
    	// be filled out. These are not required for any transaction.
    	PayPalInvoiceData invoice = new PayPalInvoiceData();
    	BigDecimal total = BigDecimal.ZERO;
    	invoice.setShipping(BigDecimal.ZERO);         	// Sets the shipping amount.
        Log.e(TAG ,"Goods "+goods.size());
    	for(Goods good: goods){
            Log.e(TAG , good.name+" "+good.getPrice()+""+good.getSum());
    		BigDecimal price = new BigDecimal(good.getPrice());
            if (good.qty==0) good.qty=1;
            BigDecimal sum = new BigDecimal(good.getSum());
        	// PayPalInvoiceItem has several parameters available to it. None of these parameters is required.
        	PayPalInvoiceItem item = new PayPalInvoiceItem();
        	// Sets the name of the item.
        	item.setName(good.name);
        	// Sets the ID. This is any ID that you would like to have associated with the item.
        	item.setID(String.valueOf(good.server_id));
        	// Sets the total price which should be (quantity * unit price). The total prices of all PayPalInvoiceItem should add up
        	// to less than or equal the subtotal of the payment.
        	item.setTotalPrice(sum);
        	// Sets the unit price.
        	item.setUnitPrice(price);
        	// Sets the quantity.

        	item.setQuantity(good.qty);
        	// Add the PayPalInvoiceItem to the PayPalInvoiceData. Alternatively, you can create an ArrayList<PayPalInvoiceItem>
        	// and pass it to the PayPalInvoiceData function setInvoiceItems().
        	invoice.getInvoiceItems().add(item);
        	total = total.add( sum );
    	}

        invoice.setTax(total.multiply(TAX));         	// Sets the tax amount.

        // Sets the amount of the payment, not including tax and shipping amounts.
        payment.setSubtotal(total.subtract(total.multiply(TAX)));

        Log.e(TAG , "Subtotal "+String.valueOf(total.subtract(total.multiply(TAX))));
    	// Sets the PayPalPayment invoice data.
    	payment.setInvoiceData(invoice);
        Log.e(TAG , "TAX "+String.valueOf(invoice.getTax()));
    	// Sets the merchant name. This is the name of your Application or Company.
    	payment.setMerchantName(MERCHANT);
    	// Sets the Custom ID. This is any ID that you would like to have associated with the payment.
    	payment.setCustomID("8873482296");
    	// Sets the Instant Payment Notification url. This url will be hit by the PayPal server upon completion of the payment.
    	payment.setIpnUrl("http://www.exampleapp.com/ipn");
    	// Sets the memo. This memo will be part of the notification sent by PayPal to the necessary parties.
    	payment.setMemo(MEMO);
    	
    	return payment;
	}


    /**
     * Create a PayPalPayment which is used for simple payments.
     *
     * @return Returns a PayPalPayment.
     */

    private PayPalPayment exampleSimplePayment() {
        ArrayList<Goods> goods = getActivity().getIntent().getParcelableArrayListExtra(VisaListActivity.GOODS);
        // Create a basic PayPalPayment.
        PayPalPayment payment = new PayPalPayment();
        // Sets the currency type for this payment.
        payment.setCurrencyType("USD");
        // Sets the recipient for the payment. This can also be a phone number.
        payment.setRecipient("example-merchant-1@paypal.com");
        // Sets the amount of the payment, not including tax and shipping amounts.
        payment.setSubtotal(new BigDecimal("89.68"));
        // Sets the payment type. This can be PAYMENT_TYPE_GOODS, PAYMENT_TYPE_SERVICE, PAYMENT_TYPE_PERSONAL, or PAYMENT_TYPE_NONE.
        payment.setPaymentType(PayPal.PAYMENT_TYPE_GOODS);

        // PayPalInvoiceData can contain tax and shipping amounts. It also contains an ArrayList of PayPalInvoiceItem which can
        // be filled out. These are not required for any transaction.
        PayPalInvoiceData invoice = new PayPalInvoiceData();
        // Sets the tax amount.
        invoice.setTax(new BigDecimal("22.42"));
        // Sets the shipping amount.
        invoice.setShipping(BigDecimal.ZERO);

        // PayPalInvoiceItem has several parameters available to it. None of these parameters is required.
        PayPalInvoiceItem item1 = new PayPalInvoiceItem();
        // Sets the name of the item.
        item1.setName(goods.get(0).name);
        // Sets the ID. This is any ID that you would like to have associated with the item.
        item1.setID(String.valueOf(goods.get(0).id));
        // Sets the total price which should be (quantity * unit price). The total prices of all PayPalInvoiceItem should add up
        // to less than or equal the subtotal of the payment.
        item1.setTotalPrice(new BigDecimal(goods.get(0).getSum()));
        // Sets the unit price.
        item1.setUnitPrice(new BigDecimal(goods.get(0).getPrice()));
        // Sets the quantity.
        item1.setQuantity(goods.get(0).qty);
        // Add the PayPalInvoiceItem to the PayPalInvoiceData. Alternatively, you can create an ArrayList<PayPalInvoiceItem>
        // and pass it to the PayPalInvoiceData function setInvoiceItems().
        invoice.getInvoiceItems().add(item1);



        // Sets the PayPalPayment invoice data.
        payment.setInvoiceData(invoice);
        // Sets the merchant name. This is the name of your Application or Company.
        payment.setMerchantName("The Gift Store");
        // Sets the Custom ID. This is any ID that you would like to have associated with the payment.
        payment.setCustomID("8873482296");
        // Sets the Instant Payment Notification url. This url will be hit by the PayPal server upon completion of the payment.
        payment.setIpnUrl("http://www.exampleapp.com/ipn");
        // Sets the memo. This memo will be part of the notification sent by PayPal to the necessary parties.
        payment.setMemo("Hi! I'm making a memo for a simple payment.");

        return payment;
    }

}
