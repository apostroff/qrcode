package com.exadel.tests.qrcode_scaner.adapters;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.widget.CursorAdapter;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.dialogs.QRCodeDialog;
import com.exadel.tests.qrcode_scaner.domain.PayCard;

public class PaidHistoryAdapter extends CursorAdapter {
	
	public static class ViewHolder {
        public TextView tvCard;
        public TextView tvDate;
        public TextView tvName;
        public TextView tvDesc;
        public TextView tvPrice;
        public TextView tvQty;
        public TextView tvSum;

        public TextView tvUrl;
        
        
    }
	
	/**
	 * store check box values in array
	 */

	
	public PaidHistoryAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
	}
	public PaidHistoryAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
	}

	@Override
	public void changeCursor(Cursor cursor) {
		super.changeCursor(cursor);
	}
	
	
	//{PAID_ID,PAID_NAME,PAID_DESC,PAID_PRICE,PAID_QTY,PAID_URL,PAID_CARD,PAID_DATE};
	@Override
	public void bindView(View view, Context context, Cursor cursor) {

		ViewHolder holder = (ViewHolder) view.getTag();
		 
		holder.tvName.setText(cursor.getString(1));
		holder.tvDesc.setText(cursor.getString(2));
		holder.tvPrice.setText(String.valueOf(cursor.getFloat(3)/100f));
        holder.tvQty.setText(String.valueOf(cursor.getInt(4)));
        holder.tvSum.setText(String.valueOf(cursor.getInt(3)/100f*cursor.getInt(4)));
		SpannableString content = new SpannableString(cursor.getString(5));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        holder.tvUrl.setText(content);
		
		String last4 = cursor.getString(6);
		if (PayCard.PAYPAL.equals(last4))
			holder.tvCard.setText(PayCard.PAYPAL);
		else 	
			holder.tvCard.setText("********"+last4);
		
	    holder.tvDate.setText(DateFormat.format(QRCodeDialog.DATE_FORMAT,cursor.getLong(7)).toString()  );
	}

	/**
	 * Create view and set holder to its tag
	 */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup container) {
		LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.already_row,null,true);

        ViewHolder holder = new ViewHolder();
        
        holder.tvName = (TextView) rowView.findViewById(R.id.tv_qr_name_value);
        holder.tvDesc = (TextView) rowView.findViewById(R.id.tv_qr_desc_value);
        holder.tvQty = (TextView) rowView.findViewById(R.id.tv_qr_row_qty);
        holder.tvPrice = (TextView) rowView.findViewById(R.id.tv_qr_row_price);
        holder.tvSum = (TextView) rowView.findViewById(R.id.tv_qr_row_sum);

        holder.tvUrl = (TextView) rowView.findViewById(R.id.tv_qr_url_value);
        holder.tvUrl.setOnClickListener(onUrlClick);
        
        holder.tvCard	= (TextView) rowView.findViewById(R.id.tv_qr_cardnumber_value);
        holder.tvDate		= (TextView) rowView.findViewById(R.id.tv_qr_date_value);
        rowView.setTag(holder);
		return rowView;
	}
	
	OnClickListener onUrlClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String url = ((TextView)v).getText().toString();
			if (url.length()==0) return;
			if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
			
			Intent intent =new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			PaidHistoryAdapter.this.mContext.startActivity(intent);
		}
	};  
			
}
