package com.exadel.tests.qrcode_scaner.dialogs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.exadel.tests.qrcode_scaner.R;
/***
 * display some kind of help tooltip 
 * @author apai
 *
 */
public class HelpDialog extends DisFragmentDialog {
	public static final String TAG = "HLP";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE , R.style.MyDialogTheme);
    }


    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.helpdialog, container , false);
		v.findViewById(R.id.bt_close).setOnClickListener(onClose);
		getDialog().setTitle(R.string.help_title);
		return v;
	}

	OnClickListener onClose = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			dismiss();
			
		}
	};
	

}
