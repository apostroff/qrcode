package com.exadel.tests.qrcode_scaner.dialogs;

import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.widget.Toast;

public class DisFragmentDialog extends DialogFragment {
	public static interface DismissListener{
		public void onDismiss();
	}
	
	private DismissListener dismissListener;
	public void setDismissListener(DismissListener listener){
		dismissListener=listener;
	}
	@Override
	public void dismiss(){
	//	if (dismissListener!=null) dismissListener.onDismiss();
		super.dismiss();
	}
	@Override
	public void onCancel(DialogInterface dialog) {
		if (dismissListener!=null) dismissListener.onDismiss();
		super.onCancel(dialog);
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		if (dismissListener!=null) dismissListener.onDismiss();
		super.onDismiss(dialog);
	}
	public void showToast( String str_msg){
		Toast msg = Toast.makeText(getActivity(), str_msg, Toast.LENGTH_LONG);
		msg.setGravity(Gravity.CENTER, 0, 0);
		msg.show();
	}
}
