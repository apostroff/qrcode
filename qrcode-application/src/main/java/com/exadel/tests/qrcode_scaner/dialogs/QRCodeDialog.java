package com.exadel.tests.qrcode_scaner.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.*;
import com.exadel.tests.qrcode_scaner.BuildConfig;
import com.exadel.tests.qrcode_scaner.R;
import com.exadel.tests.qrcode_scaner.VisaListActivity;
import com.exadel.tests.qrcode_scaner.domain.Goods;
import org.apache.http.impl.cookie.BasicMaxAgeHandler;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.util.ArrayList;
/****
 * dialog to show either qr code or parsed data
 * 
 * @author Apai
 *
 */
public class QRCodeDialog extends DisFragmentDialog {
	public static final String REST_URL = "https://88.198.170.125/goods/";
	public static final String TAG = "QRCD";
    public static final String GOODS = "Goods";
	public static final String QRCODE = "qrcode";
	public static final String DATE_FORMAT = "MM/dd/yy h:mmaa";
	Button btSave , btCancel , btPay;
	TextView tvQRcode , tvDate;
	TextView tvName , tvDesc , tvPrice , tvUrl;
    EditText edQty;
    ProgressBar mProgressBar , mProgressBarImage;
    ImageView mGoodsImage;
	// labels need to hide when qr code isnt parsed
	final View[] tvLabesls = new View[7];
	EditText edComment;
	long currentDate;
	private Goods record;
    LinearLayout mGoodDataLayOut;
	
	GetGoodsRestFromRest getGoodsThread;
    DownLoadGoodsImage  mDownloadImage;
	
	/**
	 * fabric method to create new instance
	 * @param qrCode
	 * @return
	 */
	public static  QRCodeDialog newInstance(String qrCode){
		QRCodeDialog dlg = new QRCodeDialog();
		Bundle prms = new Bundle();
		prms.putString(QRCODE, qrCode);// store qr code string
		dlg.setArguments(prms);
		return dlg;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.setRetainInstance(true);
		setStyle(STYLE_NO_TITLE,R.style.MyDialogTheme);
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.new_good_dialog, container, false);

		btSave  = (Button)v.findViewById(R.id.bt_save);
		btSave.setOnClickListener(onSave);
		
		btCancel  = (Button)v.findViewById(R.id.bt_cancel);
		btCancel.setOnClickListener(onCancel);
		
		btPay = (Button)v.findViewById(R.id.bt_buy);
		btPay.setOnClickListener(onBuy);
		//TODO
		tvQRcode  = (TextView)v.findViewById(R.id.tv_newgood_clean_qrcode);

        mGoodDataLayOut =(LinearLayout)v.findViewById(R.id.ll_newgood_good_data);

        v.findViewById(R.id.bt_newgood_plus).setOnClickListener(onPlus);
        v.findViewById(R.id.bt_newgood_minus).setOnClickListener(onMinus);

		tvDate = (TextView)v.findViewById(R.id.tv_newgood_date);
		edComment = (EditText)v.findViewById(R.id.ed_newgood_comment);
		tvName  = (TextView)v.findViewById(R.id.tv_newgood_name);
		tvDesc  = (TextView)v.findViewById(R.id.tv_newgood_desc);
		tvPrice = (TextView)v.findViewById(R.id.tv_newgood_price);
		tvUrl = (TextView)v.findViewById(R.id.tv_newgood_url);
        mProgressBar = (ProgressBar)v.findViewById(R.id.pb_newgood_progressBar);
        edQty = (EditText)v.findViewById(R.id.ed_newgood_qty);
        mGoodsImage = (ImageView)v.findViewById(R.id.im_newgood_image);
        mProgressBarImage = (ProgressBar)v.findViewById(R.id.pb_image_progress);
        setWindowParams();// set orientation on activity screen
        setGoodsDataVisibility(View.GONE);
        loadGoods(); // set content into ui controls
/*
		loadLabels(v );// load static labels into array
        showQrCode(); // hide buttons buy and save and goods textviews

*/

		return v;
	}
	private void setWindowParams(){
		getDialog().setTitle(R.string.qrcode);
		Window window= getDialog().getWindow();
		window.setGravity(Gravity.TOP);
		window.setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
	}
	
	/**
	 * init array of labels
	 * @param v
	 */
	private void loadLabels(View v ){
		tvLabesls[0]  = v.findViewById(R.id.tv_qr_name_label);
		tvLabesls[1]  = v.findViewById(R.id.tv_qr_desc_label);
		tvLabesls[2]  = v.findViewById(R.id.tv_qr_price_label);
		tvLabesls[3]  = v.findViewById(R.id.tv_qr_url_label);
		tvLabesls[4]  = v.findViewById(R.id.tv_comment);
		tvLabesls[5]  = v.findViewById(R.id.tv_date);
        tvLabesls[6]  = v.findViewById(R.id.tv_qr_qty_label);
	}
	/***
	 * Set content to UI controls
	 */
	private void loadGoods() {

		Bundle prms = getArguments();
		currentDate = System.currentTimeMillis();
		int id ;
		String rest_service ;
		try {
			JSONObject jsobject  = new JSONObject(prms.getString(QRCODE));
			id = jsobject.getInt("id");
			rest_service =  jsobject.getString("url");
			getGoodsThread = new GetGoodsRestFromRest(rest_service, id);
			getGoodsThread.start();
			
		} catch (JSONException e) {// on error- show qrcode and hide progress bar
			e.printStackTrace();
            tvQRcode.setText(prms.getString(QRCODE));
            mProgressBar.setVisibility(View.GONE);
			return ;
		}
		
		
	}
	
	/***
	 * start browser after click on url textview
	 */
	OnClickListener onUrlClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String url = ((TextView)v).getText().toString();
			if (url.length()==0) return;
			if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
			
			Intent intent =new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			getActivity().startActivity(intent);
		}
	};  

	/****
	 *  onsave button click
	 *  save good in database
	 */
	OnClickListener onSave = new OnClickListener() {
		@Override
		public void onClick(View v) {
            record.qty=getQtyFromEditText();
            record.comment = edComment.getText().toString();
            Uri res=null;
            try{
                res=record.save(getActivity());
            }catch(IllegalArgumentException e){
                PromtDialog dlg = PromtDialog.newInstance(getResources().getString(R.string.good_exists));
                dlg.show(getActivity().getSupportFragmentManager(),PromtDialog.TAG );
                return;
            }

			if (res!=null)
				showToast("Goods was added successfully");
            else
                showToast("Some problem was occurred during save");
			dismiss();
		}
	};
	/***
	 * oncancel button click
	 * simply close dialog
	 */
	OnClickListener onCancel = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mDownloadImage!=null&&mDownloadImage.isAlive()) mDownloadImage.interrupt();
            dismiss();
		}
	};
	/***
	 * onbuy button click
	 */
	OnClickListener onBuy = new OnClickListener() {
		@Override
		public void onClick(View v) {
			dismiss();
			Intent intent = new Intent(getActivity() , VisaListActivity.class);
			ArrayList<Goods> args = new ArrayList<Goods>();
            record.qty=getQtyFromEditText();
			args.add(record);
			intent.putParcelableArrayListExtra(VisaListActivity.GOODS, args);
			startActivity(intent);
		}
	};

    private void setGoodsDataVisibility(int visibility){
        mGoodDataLayOut.setVisibility(visibility);
        tvUrl.setVisibility(visibility);
        btPay.setVisibility(visibility);
        btSave.setVisibility(visibility);
        edComment.setVisibility(visibility);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT ,
                RelativeLayout.LayoutParams.WRAP_CONTENT);


        if (visibility==View.GONE||visibility==View.INVISIBLE){
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);

        }else{
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            params.addRule(RelativeLayout.LEFT_OF, R.id.bt_save);
            params.setMargins(0,0, (int) getResources().getDimension(R.dimen.dlg_10dp),0);
        }


        btCancel.setLayoutParams(params);

    }

    /**
     * set SSL request to trust every token
     */
    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            onErrorGoodsLoad(e.toString());
            e.printStackTrace();
        }
    }




    /***
	 *  class used for get goods details by its id 
	 *  from rest web service 
	 * @author apai
	 *
	 */
	private class GetGoodsRestFromRest extends Thread {
		String restURL;
		int goodsID;
		public GetGoodsRestFromRest(String url, int id ){
			restURL = url;
			goodsID = id ;
		}

        /**
         * on error - show source qr code and hide progress bar
         */


		public void run(){
			String imageurl = restURL+ goodsID;

            try {
                trustEveryone();
			    URL url = new URL(imageurl);
			    URLConnection urlConn = url.openConnection();
			    HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			    httpConn.connect();
			    
			    BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
				int read = 0;
				char[] mData = new char[1024];
				final StringBuilder builder = new StringBuilder();
				while ((read = reader.read(mData)) >= 0 && !isInterrupted()) 		{
					if(read > 0) 
						builder.append((String.valueOf(mData).substring(0, read)));
				}
				reader.close();
			    getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						onGoodsLoaded(builder.toString());
					}
				});
				
			 } catch (MalformedURLException e) {
				 onError(e.toString());
				 if (BuildConfig.DEBUG) e.printStackTrace();

				 return ;
			 } catch (IOException e) {
				 onError(e.toString());
				 if (BuildConfig.DEBUG) e.printStackTrace();

				 return ;
			 }
		}
		
		private void onError(final String error){
		    getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() { onErrorGoodsLoad(error);		}
		    });
		}
		
	}
	public void onErrorGoodsLoad(String err){

		showToast(err);

        tvQRcode.setText(QRCodeDialog.this.getArguments().getString(QRCODE));
        mProgressBar.setVisibility(View.GONE);

		//showQrCode();// hide parsed data controls

	}
	
	// TODO 
	public void onGoodsLoaded(String jsonString){
		
		try {
			record = new Goods(jsonString, currentDate, "");
            mProgressBar.setVisibility(View.GONE);
            tvQRcode.setVisibility(View.GONE);
            setGoodsDataVisibility(View.VISIBLE);

			if (record!=null){
				tvName.setText(record.name);
				tvDesc.setText(record.desc);
				tvPrice.setText(String.valueOf(record.getPrice()));
				// set underline style to textview
				SpannableString content = new SpannableString(record.detailUrl);
		        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		        tvUrl.setText(content);
		        tvUrl.setOnClickListener(onUrlClick);

                String urlImage = REST_URL+"pictures/"+record.server_id+"/"+record.imageName;
                mDownloadImage = new DownLoadGoodsImage(urlImage);
                mDownloadImage.start();
			}
			tvDate.setText(DateFormat.format(DATE_FORMAT,currentDate  ).toString()  );
		} catch (JSONException e) {
			onErrorGoodsLoad(e.toString());
			if (BuildConfig.DEBUG) e.printStackTrace();
		}
		
		
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		if (getGoodsThread!=null&&getGoodsThread.isAlive()) getGoodsThread.interrupt();
		super.onCancel(dialog);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		if (getGoodsThread!=null&&getGoodsThread.isAlive()) getGoodsThread.interrupt();
		super.onDismiss(dialog);
	}

    /**
     * set qty to record from edit box in form
     * @return
     */
    private int getQtyFromEditText(){
        int qty;
        try {
             qty =  Integer.decode(edQty.getText().toString());
        }catch(NumberFormatException e){
            qty=1;
        }
        return qty;
    }

    private class DownLoadGoodsImage extends  Thread{
        private String mImageUrl;
        public DownLoadGoodsImage(String url){
            mImageUrl  = url;
        }

        public void run(){
            Log.e(TAG ,mImageUrl );

            try {
                trustEveryone();
                URL url = new URL(mImageUrl);
                URLConnection urlConn = url.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection) urlConn;
                httpConn.connect();


                final Bitmap image  = BitmapFactory.decodeStream(httpConn.getInputStream());

                if (!isInterrupted())
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onImageDownLoaded(image);
                        }
                    });

            } catch (MalformedURLException e) {
                onError(e.toString());
                if (BuildConfig.DEBUG) e.printStackTrace();

                return ;
            } catch (IOException e) {
                onError(e.toString());
                if (BuildConfig.DEBUG) e.printStackTrace();

                return ;
            }
        }
        private void onError(final String error){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() { onErrorImageLoad(error);		}
            });
        }

    }

    private void   onErrorImageLoad(String error){

    }
    private void onImageDownLoaded(Bitmap image){
        record.imageBitmap = image;
        mProgressBarImage.setVisibility(View.GONE);
        mGoodsImage.setLayoutParams(new android.widget.FrameLayout.LayoutParams(mGoodsImage.getMeasuredWidth(), mGoodsImage.getMeasuredWidth()));
        mGoodsImage.setImageBitmap(image);

    }

    private OnClickListener onPlus  = new OnClickListener() {
        @Override
        public void onClick(View view) {
            int newQty = getQtyFromEditText()+1;
            edQty.setText(String.valueOf(newQty));
        }
    };

    private OnClickListener onMinus = new OnClickListener() {
        @Override
        public void onClick(View view) {
            int newQty = Math.max(getQtyFromEditText()-1,1);
            edQty.setText(String.valueOf(newQty));
        }
    };

}
