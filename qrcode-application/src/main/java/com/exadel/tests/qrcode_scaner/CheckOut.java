package com.exadel.tests.qrcode_scaner;

import android.content.Context;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.exadel.tests.qrcode_scaner.domain.Goods;

import java.util.ArrayList;

/**
 * fragment for display billing with total paid sum
 * @author apai
 *
 */

public class CheckOut extends Fragment{
	public static final String TAG = "CHF";
    ItemsArrayAdapter mAdapter ;   // adapter for goods items
    ArrayList<Goods> mGoods=null; // goods  ,they stored by factory method
	/**
	 * factory method
	 */
	
	public static CheckOut newInstance(ArrayList<? extends Parcelable> good_list){
		CheckOut fragm = new CheckOut();
		Bundle args = new Bundle();
		args.putParcelableArrayList(VisaListActivity.GOODS, good_list);
		fragm.setArguments(args);
		return fragm;
	}
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v  = inflater.inflate(R.layout.checkout_layout , container , false);
        if (getArguments().containsKey(VisaListActivity.GOODS) )
            mGoods = getArguments().getParcelableArrayList(VisaListActivity.GOODS);
        // if no arraylist was sended - show empty list
        if (mGoods ==null)    mGoods = new ArrayList<Goods>();



        mAdapter = new ItemsArrayAdapter(getActivity() , R.layout.checkout_row_layout , mGoods);
        final ListView  listView = (ListView)v.findViewById(R.id.lv_checkout_items);
        listView.setAdapter(mAdapter);
        listView.setClickable(false);
        listView.setSelected(false);
        listView.setEnabled(false);
        listView.setOnItemClickListener(null);

        float totalSum =0;
        for (Goods good : mGoods) totalSum+=good.getSum();

        final TextView tvTotalSum = (TextView)v.findViewById(R.id.tv_checkout_totalsum);
        tvTotalSum.setText(String.valueOf(totalSum));

        return v;

	}


    /**
     * Array Adappeer for ListView
     * List of goods
     * columns :
     * name | qty | price | sum
     */

    public static class ItemsArrayAdapter extends  ArrayAdapter<Goods>{
        private ArrayList<Goods> items;
        private Context mContext;
        public static class ViewHolder{
            TextView tvName;
            TextView tvQty;
            TextView tvPrice;
            TextView tvSum;
        }

        public ItemsArrayAdapter(Context context, int textViewResourceId, ArrayList<Goods> items) {
            super(context, textViewResourceId, items);
            this.items = items;
            mContext = context;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //	super.getView(position, convertView, parent);
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.checkout_row_layout, null);
                ViewHolder holder = new ViewHolder();
                holder.tvName  = (TextView)v.findViewById(R.id.tv_checkoutrow_name);
                holder.tvQty  = (TextView)v.findViewById(R.id.tv_checkoutrow_qty);
                holder.tvPrice  = (TextView)v.findViewById(R.id.tv_checkoutrow_price);
                holder.tvSum  = (TextView)v.findViewById(R.id.tv_checkoutrow_sum);
                v.setTag(holder);
            }

            ViewHolder holder = (ViewHolder)v.getTag();
            Goods goods = items.get(position);

            holder.tvName.setText(goods.name);
            holder.tvQty.setText(String.valueOf(goods.qty));
            holder.tvPrice.setText(String.valueOf(goods.getPrice()));
            holder.tvSum.setText(String.valueOf(goods.getSum()));
            return  v;
        }
    }


}
