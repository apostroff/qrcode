package com.exadel.tests.qrcode_scaner.database;


import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
/**
 * typical content provider  nothing special
 * @author apai
 *
 */
public class QrContentProvider extends ContentProvider {
	public static final boolean DEBUG = false;
	public static final String TAG = "NardsContentProvider";
	private SQLiteDatabase db;
	
	public static final String AUTHORITY = "com.exadel.tests.qrcode_scaner";
	public static final Uri CONTENT_URI_BASE = Uri.parse("content://" + AUTHORITY);
	
	public static final Uri CONTENT_URI_VISA =  CONTENT_URI_BASE.buildUpon().appendEncodedPath("visa").build();
	public static final Uri CONTENT_URI_GOODS =  CONTENT_URI_BASE.buildUpon().appendEncodedPath("goods").build();
	public static final Uri CONTENT_URI_PAID =  CONTENT_URI_BASE.buildUpon().appendEncodedPath("paid").build();
    

	public static final String GOODS_CONTENT_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.goods";
	public static final String GOODS_CONTENT_ITEM_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.goods_item";
	
	public static final String PAID_CONTENT_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.paid";
	public static final String PAID_CONTENT_ITEM_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.paid_item";
	
	public static final String VISA_CONTENT_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.visas";
	public static final String VISA_CONTENT_ITEM_TYPE  = "vnd.android.cursor.dir/vnd.qrcodescann.visa";
	
	
	public static final String GOODS_ID = "_id";
	public static final String GOODS_QR = "QR_CODE";
	public static final String GOODS_COMMENT = "COMMENT";
	public static final String GOODS_SCANNED_DATE = "SCANNED_DATE";
	public static final String GOODS_NAME = "GOOD_NAME" ;
	public static final String GOODS_DESC = "GOOD_DESC";
	public static final String GOODS_PRICE="PRICE" ;
    public static final String GOODS_QTY= "QTY";
	public static final String GOODS_URL = "GOOD_URL" ;
    public static final String GOODS_SERVER_ID = "SERVER_ID";
    public static final String GOODS_IMAGE = "IMAGE";
	
	public static final String[] GOODS_COLUMNS = {GOODS_ID, GOODS_QR,GOODS_COMMENT ,GOODS_SCANNED_DATE ,
		GOODS_NAME , GOODS_DESC , GOODS_PRICE,GOODS_QTY , GOODS_URL,GOODS_SERVER_ID , GOODS_IMAGE};
	
	
	public static final String VISA_ID = "_id";
	public static final String VISA_NUMBER = "CARD_NUMBER";
	public static final String VISA_YEAR = "VALID_YEAR";
	public static final String VISA_MONTH = "VALID_MONTH";
	public static final String VISA_OWNER = "OWNER";
	public static final String VISA_CW2 = "CW2";
	public static final String VISA_TYPE = "CARD_TYPE";
	public static final String VISA_LAST_NUMBER = "LAST_NUMBER";
	public static final String VISA_LIMIT = "MONEY_LIMIT";
	
	public static final String[] VISA_COLUMNS = {VISA_ID,VISA_NUMBER,VISA_YEAR,VISA_MONTH,VISA_OWNER,
			VISA_CW2, VISA_TYPE, VISA_LAST_NUMBER , VISA_LIMIT} ; 
	
	public static final String PAID_ID = "_id";
	public static final String PAID_NAME = "GOOD_NAME" ; 
	public static final String PAID_DESC = "GOOD_DESC";  
	public static final String PAID_PRICE="PRICE" ;
    public static final String PAID_QTY= "QTY";
	public static final String PAID_URL = "GOOD_URL" ;
	public static final String PAID_CARD = "LAST_NUMBER"; 
	public static final String PAID_DATE = "PAID_DATE";
    public static final String PAID_IMAGE = "IMAGE";
	
	public static final String[] PAID_COLUMNS={PAID_ID,PAID_NAME,PAID_DESC,PAID_PRICE,PAID_QTY,
            PAID_URL,PAID_CARD,PAID_DATE,PAID_IMAGE};
	
	
	private static final UriMatcher sUriMatcher = buildUriMatcher();
	private static final int INCOMING_GOODS_LIST = 1;
	private static final int INCOMING_GOODS_ROW = 2;
	private static final int INCOMING_VISA_LIST = 3;
	private static final int INCOMING_VISA_CARD = 4;
	private static final int INCOMING_PAID_LIST = 5;
	private static final int INCOMING_PAID_ROW = 6;
	
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, "goods", INCOMING_GOODS_LIST);
        matcher.addURI(AUTHORITY, "goods/*", INCOMING_GOODS_ROW);
        
        matcher.addURI(AUTHORITY, "visa", INCOMING_VISA_LIST);
        matcher.addURI(AUTHORITY, "visa/*", INCOMING_VISA_CARD);
        
        matcher.addURI(AUTHORITY, "paid", INCOMING_PAID_LIST);
        matcher.addURI(AUTHORITY, "paid/*", INCOMING_PAID_ROW);
        return matcher;
    }
    
	
	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		
		int count;
		switch (sUriMatcher.match(uri)) {
		case INCOMING_GOODS_LIST:
			count = db.delete(DataHelper.TABLE_GOODS, where, whereArgs);
			break;
		case INCOMING_GOODS_ROW :
			count  =db.delete(DataHelper.TABLE_GOODS, GOODS_ID+"=?",  new String[]{uri.getLastPathSegment()});
			Log.e("delete "+count , uri.toString());
			break;
		case INCOMING_VISA_LIST:
			count = db.delete(DataHelper.TABLE_VISA, where, whereArgs);
			break;
		case INCOMING_VISA_CARD : 
			count  =db.delete(DataHelper.TABLE_VISA, VISA_ID+"=?",  new String[]{uri.getLastPathSegment()});
			break;
		case INCOMING_PAID_LIST:
			count = db.delete(DataHelper.TABLE_PAID, where, whereArgs);
			break;
		case INCOMING_PAID_ROW : 
			count  =db.delete(DataHelper.TABLE_PAID, PAID_ID+"=?",  new String[]{uri.getLastPathSegment()});
			break;	
			
		default:
			throw new IllegalArgumentException("Failed to deleled "+uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch(sUriMatcher.match(uri)){
		case INCOMING_GOODS_LIST :
			return GOODS_CONTENT_TYPE;
		case INCOMING_GOODS_ROW :
			return GOODS_CONTENT_ITEM_TYPE;
		case INCOMING_VISA_LIST : 
			return VISA_CONTENT_TYPE;
		case INCOMING_VISA_CARD : 
			return VISA_CONTENT_ITEM_TYPE;
		case INCOMING_PAID_LIST : 
			return PAID_CONTENT_TYPE;
		case INCOMING_PAID_ROW: 
			return PAID_CONTENT_ITEM_TYPE;
		default:
	        throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
	
	}
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder) {

		final int match = sUriMatcher.match(uri);
		switch(match){
			case INCOMING_GOODS_LIST :{
				final Cursor cursor = db.query(DataHelper.TABLE_GOODS,projection ,  selection, selectionArgs, null, null, "1");
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
			case INCOMING_GOODS_ROW : {
				final Cursor cursor = db.query(DataHelper.TABLE_GOODS, GOODS_COLUMNS, GOODS_ID+"=?",
						new String[]{uri.getLastPathSegment()},	null, null, null);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
			case INCOMING_VISA_LIST :{ 
				final Cursor cursor = db.query(DataHelper.TABLE_VISA,VISA_COLUMNS ,  selection, selectionArgs, null, null, "1");
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
			case INCOMING_VISA_CARD : { 	
				final Cursor cursor = db.query(DataHelper.TABLE_VISA, VISA_COLUMNS, VISA_ID+"=?",
						new String[]{uri.getLastPathSegment()},	null, null, null);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
			case INCOMING_PAID_LIST:{ 
				final Cursor cursor = db.query(DataHelper.TABLE_PAID,PAID_COLUMNS ,  selection, selectionArgs, null, null, "1");
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
			case INCOMING_PAID_ROW : { 	
				final Cursor cursor = db.query(DataHelper.TABLE_PAID, PAID_COLUMNS, PAID_ID+"=?",
						new String[]{uri.getLastPathSegment()},	null, null, null);
				cursor.setNotificationUri(getContext().getContentResolver(), uri);
				return cursor;
			}
		}
		return null;
	}
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int matched_uri = sUriMatcher.match(uri);
		if (matched_uri!=INCOMING_VISA_LIST&&
				matched_uri!=INCOMING_GOODS_LIST&&
				matched_uri!=INCOMING_PAID_LIST){
			throw new IllegalArgumentException("Unknown URI "+uri);
		}
		ContentValues v;
		if (values==null)
			v = new ContentValues();
		else 
			v = new ContentValues(values) ;
		long rowID =0;
		if (matched_uri==INCOMING_GOODS_LIST){
			if (!v.containsKey(GOODS_QR))
				throw new SQLException("Fail to insert! QrCode is needed");
			rowID = db.insert(DataHelper.TABLE_GOODS, GOODS_QR, v);
		}
		if (matched_uri==INCOMING_PAID_LIST){ 
			rowID = db.insert(DataHelper.TABLE_PAID, null, v);
		}
		if (matched_uri==INCOMING_VISA_LIST){ 
			if (!v.containsKey(VISA_NUMBER))
				throw new SQLException("Fail to insert! QrCode is needed");			
			rowID = db.insert(DataHelper.TABLE_VISA, VISA_NUMBER, v);
		}
		
		if (rowID>0){
			Uri insertedUri  = ContentUris.withAppendedId(CONTENT_URI_BASE, rowID);
			getContext().getContentResolver().notifyChange(uri, null);
			return insertedUri;
		}
		throw new SQLException("Fail to insert row into "+uri);	
		
	}	
	@Override
	public boolean onCreate() {
		 db = new DataHelper(getContext()).getWritableDatabase();
		 return true;
	}

	

	@Override
	public int update(Uri uri, ContentValues values, String selection,String[] selectionArgs) {
		int count;
		switch (sUriMatcher.match(uri)) {
		case INCOMING_VISA_LIST:
			count = db.update(DataHelper.TABLE_VISA, values, selection, selectionArgs);
			break;
		case INCOMING_VISA_CARD: 
			count = db.update(DataHelper.TABLE_VISA, values, VISA_ID+"=?", new String[]{uri.getLastPathSegment()});
			break;
		case INCOMING_GOODS_LIST:
			count = db.update(DataHelper.TABLE_GOODS, values, selection, selectionArgs);
			break;
		case INCOMING_GOODS_ROW:
			count = db.update(DataHelper.TABLE_GOODS, values, GOODS_ID+"=?", new String[]{uri.getLastPathSegment()});
			break;
			
		case INCOMING_PAID_LIST:
			count = db.update(DataHelper.TABLE_PAID, values, selection, selectionArgs);
			break;
		case INCOMING_PAID_ROW: 
			count = db.update(DataHelper.TABLE_PAID, values, PAID_ID+"=?", new String[]{uri.getLastPathSegment()});
			break;			

		default:
			throw new IllegalArgumentException("Failed to update "+uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
